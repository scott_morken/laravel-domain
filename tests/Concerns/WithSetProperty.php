<?php

namespace Tests\Smorken\Domain\Concerns;

trait WithSetProperty
{
    public function setProperty(object $o, string $property, mixed $value): void
    {
        $ref = new \ReflectionClass($o);
        if ($ref->hasProperty($property)) {
            $prop = $ref->getProperty($property);
            $prop->setValue($o, $value);

            return;
        }
        $o->$property = $value;
    }
}
