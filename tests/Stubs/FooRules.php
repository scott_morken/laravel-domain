<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Stubs;

class FooRules
{
    public static function rules(): array
    {
        return [
            'foo' => 'required',
        ];
    }
}
