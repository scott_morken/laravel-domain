<?php

namespace Tests\Smorken\Domain\Stubs\Repositories;

use Smorken\Domain\Repositories\Contracts\Repository;

interface RepositoryContractStub extends Repository
{
    public function __invoke(...$args): mixed;
}
