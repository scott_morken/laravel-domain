<?php

namespace Tests\Smorken\Domain\Stubs\Repositories;

use Smorken\Domain\Repositories\Repository;

class RepositoryStub extends Repository implements RepositoryContractStub
{
    public function __invoke(...$args): mixed
    {
        if ($args) {
            return $args;
        }

        return 'result';
    }
}
