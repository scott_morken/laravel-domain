<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Stubs\Repositories;

use Smorken\Domain\Actions\Action;

class ActionStub extends Action
{
    public function __invoke(...$args): mixed
    {
        if ($args) {
            return $args;
        }

        return 'result';
    }
}
