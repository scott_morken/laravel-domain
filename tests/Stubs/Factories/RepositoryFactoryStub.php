<?php

namespace Tests\Smorken\Domain\Stubs\Factories;

use Smorken\Domain\Factories\RepositoryFactory;

class RepositoryFactoryStub extends RepositoryFactory {}
