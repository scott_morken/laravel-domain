<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Stubs\Policies;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Domain\Authorization\Policies\ModelBasedPolicy;
use Smorken\Model\Contracts\Model;

class AuthorizeActionPolicy extends ModelBasedPolicy
{
    public function before(Authenticatable $user, string $ability): ?bool
    {
        if ($user->getAuthIdentifier() === 1000) {
            return true;
        }

        return null;
    }

    protected function isUserAllowedToView(Authenticatable $user, Model $model): Response
    {
        if ($user->getAuthIdentifier() === $model->user_id) {
            return $this->allow();
        }

        return parent::isUserAllowedToView($user, $model);
    }
}
