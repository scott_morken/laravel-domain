<?php

namespace Tests\Smorken\Domain\Stubs;

use Smorken\Domain\Support\Concerns\ArrayableFromPublicPropsAndMethods;

class ObjectWithPublicPropertiesStub
{
    use ArrayableFromPublicPropsAndMethods;

    public function __construct(public string $foo, protected int $bar) {}
}
