<?php

namespace Tests\Smorken\Domain\Stubs\DataTransferObjects\GatePolicies;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Domain\Authorization\Policies\ModelBasedPolicy;
use Smorken\Model\Contracts\Model;

class RetrieveGatePolicyStub extends ModelBasedPolicy
{
    protected function isUserAllowedToView(Authenticatable $user, Model $model): Response
    {
        if ($user->getAuthIdentifier() === $model->owner_id) {
            return $this->allow();
        }

        return parent::isUserAllowedToView($user, $model);
    }

    protected function isUserAllowedToViewAny(Authenticatable $user): Response
    {
        if ($user->getAuthIdentifier() === 99) {
            return $this->allow();
        }

        return parent::isUserAllowedToViewAny($user);
    }
}
