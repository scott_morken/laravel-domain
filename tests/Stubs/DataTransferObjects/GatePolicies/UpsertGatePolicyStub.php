<?php

namespace Tests\Smorken\Domain\Stubs\DataTransferObjects\GatePolicies;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Domain\Authorization\Policies\ModelBasedPolicy;
use Smorken\Model\Contracts\Model;

class UpsertGatePolicyStub extends ModelBasedPolicy
{
    protected function isUserAllowedToCreate(Authenticatable $user, ?array $attributes = null): Response
    {
        return $this->allow();
    }

    protected function isUserAllowedToUpdate(Authenticatable $user, Model $model): Response
    {
        if ($user->getAuthIdentifier() === $model->owner_id) {
            return $this->allow();
        }

        return parent::isUserAllowedToUpdate($user, $model);
    }
}
