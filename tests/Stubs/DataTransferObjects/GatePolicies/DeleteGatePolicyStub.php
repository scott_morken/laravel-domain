<?php

namespace Tests\Smorken\Domain\Stubs\DataTransferObjects\GatePolicies;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Domain\Authorization\Policies\ModelBasedPolicy;
use Smorken\Model\Contracts\Model;

class DeleteGatePolicyStub extends ModelBasedPolicy
{
    public function before(Authenticatable $user, string $ability): ?bool
    {
        if ($user->getAuthIdentifier() === 99) {
            return true;
        }

        return null;
    }

    protected function isUserAllowedToDestroy(Authenticatable $user, Model $model): Response
    {
        if ($user->getAuthIdentifier() === $model->owner_id) {
            return $this->allow();
        }

        return parent::isUserAllowedToDestroy($user, $model);
    }
}
