<?php

namespace Tests\Smorken\Domain\Stubs\Models;

use Smorken\Domain\Models\BaseEloquentModel;
use Tests\Smorken\Domain\Stubs\DataTransferObjects\RetrieveViewModelDataStub;

class RetrieveModelStub extends BaseEloquentModel
{
    protected string $dataClass = RetrieveViewModelDataStub::class;

    protected $fillable = ['owner_id'];
}
