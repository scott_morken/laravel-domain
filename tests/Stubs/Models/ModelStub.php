<?php

namespace Tests\Smorken\Domain\Stubs\Models;

use Smorken\Domain\Models\BaseEloquentModel;
use Tests\Smorken\Domain\Stubs\Builders\ModelBuilderStub;
use Tests\Smorken\Domain\Stubs\DataTransferObjects\DataStub;

class ModelStub extends BaseEloquentModel
{
    protected string $builderClass = ModelBuilderStub::class;

    protected string $dataClass = DataStub::class;

    protected $fillable = ['foo', 'bar'];
}
