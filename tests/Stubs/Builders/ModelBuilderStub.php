<?php

namespace Tests\Smorken\Domain\Stubs\Builders;

use Smorken\Model\QueryBuilders\Builder;
use Smorken\Model\QueryBuilders\Concerns\HasIdIsScope;

class ModelBuilderStub extends Builder
{
    use HasIdIsScope;
}
