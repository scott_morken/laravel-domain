<?php

namespace Tests\Smorken\Domain\Unit\Shared\Helpers;

use PHPUnit\Framework\TestCase;
use Smorken\Domain\Support\Helpers\GetPublicMethods;

class GetPublicMethodsTest extends TestCase
{
    public function test_only_public_methods(): void
    {
        $cls = new class
        {
            public function foo() {}

            public function fooWithParams(string $blah) {}

            protected function bar() {}

            public static function biz() {}

            public static function baz(string $blah) {}
        };

        $sut = new GetPublicMethods($cls);
        $this->assertEquals([
            'foo',
            'biz',
        ], $sut->get()->toArray());
    }
}
