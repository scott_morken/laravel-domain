<?php

namespace Tests\Smorken\Domain\Unit\Shared\Helpers;

use PHPUnit\Framework\TestCase;
use Smorken\Domain\Support\Helpers\GetPublicProperties;

class GetPublicPropertiesTest extends TestCase
{
    public function test_only_public_properties(): void
    {
        $cls = new class
        {
            public string $foo = 'foo';

            protected int $bar = 1;

            const FIZ = 'buz';
        };
        $sut = new GetPublicProperties($cls);
        $this->assertEquals([
            'foo',
        ], $sut->get()->toArray());
    }
}
