<?php

namespace Tests\Smorken\Domain\Unit\Shared\Concerns;

use PHPUnit\Framework\TestCase;
use Tests\Smorken\Domain\Stubs\ObjectWithPublicPropertiesStub;

class ArrayableFromPublicPropsAndMethodsTest extends TestCase
{
    public function test_with_public_properties(): void
    {
        $sut = new ObjectWithPublicPropertiesStub('foo', 99);
        $this->assertEquals([
            'foo' => 'foo',
        ], $sut->toArray());
    }
}
