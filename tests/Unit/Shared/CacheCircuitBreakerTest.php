<?php

namespace Tests\Smorken\Domain\Unit\Shared;

use Illuminate\Cache\CacheManager;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Domain\Support\CacheCircuitBreaker;
use Smorken\Domain\Support\Contracts\CircuitBreaker;

class CacheCircuitBreakerTest extends TestCase
{
    protected ?CacheManager $cacheManager = null;

    public function test_closed_when_cached(): void
    {
        $c = new class
        {
            public bool $closed = false;
        };
        $sut = $this->getSut();
        $this->getCacheManager()
            ->expects()
            ->has('circuit_breaker.test')
            ->andReturns(true);
        $sut('test', fn () => $c->closed = false, fn () => $c->closed = true, fn () => $c->closed = false);
        $this->assertTrue($c->closed);
    }

    public function test_open_circuit(): void
    {
        $c = new class
        {
            public bool $closed = false;
        };
        $sut = $this->getSut();
        $this->getCacheManager()
            ->expects()
            ->has('circuit_breaker.test')
            ->andReturns(false);
        $sut('test', fn () => $c->closed = false, fn () => $c->closed = true, fn () => $c->closed = false);
        $this->assertFalse($c->closed);
    }

    public function test_when_circuit_evaluator_is_false_rethrows_exception(): void
    {
        $c = new class
        {
            public bool $closed = false;
        };
        $sut = $this->getSut();
        $this->getCacheManager()
            ->expects()
            ->has('circuit_breaker.test')
            ->andReturns(false);
        $this->expectException(\OutOfBoundsException::class);
        $sut('test', fn () => throw new \OutOfBoundsException, fn () => $c->closed = true,
            fn (\Throwable $throwable) => $throwable instanceof \ErrorException);
    }

    public function test_when_circuit_evaluator_is_true_runs_closed_circuit(): void
    {
        $c = new class
        {
            public bool $closed = false;
        };
        $sut = $this->getSut();
        $this->getCacheManager()
            ->expects()
            ->has('circuit_breaker.test')
            ->andReturns(false);
        $this->getCacheManager()
            ->expects()
            ->set('circuit_breaker.test', true, 60);
        $sut('test', fn () => throw new \ErrorException, fn () => $c->closed = true,
            fn (\Throwable $throwable) => $throwable instanceof \ErrorException);
        $this->assertTrue($c->closed);
    }

    protected function getCacheManager(): CacheManager
    {
        if ($this->cacheManager === null) {
            $this->cacheManager = m::mock(CacheManager::class);
        }

        return $this->cacheManager;
    }

    protected function getSut(): CircuitBreaker
    {
        return new CacheCircuitBreaker;
    }

    protected function setUp(): void
    {
        parent::setUp();
        CacheCircuitBreaker::setCacheManager($this->getCacheManager());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
