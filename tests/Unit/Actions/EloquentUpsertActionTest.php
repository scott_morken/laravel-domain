<?php

namespace Tests\Smorken\Domain\Unit\Actions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User;
use Illuminate\Validation\ValidationException;
use Mockery as m;
use Smorken\Domain\Actions\EloquentUpsertAction;
use Smorken\Domain\Actions\Support\SnakeCaseKeyConvert;
use Smorken\Domain\Actions\Upsertable;
use Smorken\Domain\Models\BaseEloquentModel;
use Tests\Smorken\Domain\Stubs\DataTransferObjects\GatePolicies\UpsertGatePolicyStub;
use Tests\Smorken\Domain\Stubs\FooRules;
use Tests\Smorken\Domain\Stubs\Models\ModelStub;
use Tests\Smorken\Domain\TestBenchTestCase;

class EloquentUpsertActionTest extends TestBenchTestCase
{
    public function test_update_authorized(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 2]));
        $mockModel = m::mock(new ModelStub);
        UpsertGatePolicyStub::defineSelf($mockModel::class, $gate);
        EloquentUpsertAction::setGate($gate);
        $mockModel->shouldReceive('newInstance')->andReturnSelf();
        $mockQuery = m::mock(Builder::class);
        $mockQuery->allows()->getModel()->andReturn($mockModel);
        $sut = new class($mockModel) extends EloquentUpsertAction {};
        $modelResult = m::mock((new ModelStub)->forceFill(['id' => 1, 'owner_id' => 2]))->makePartial();
        $mockModel->shouldReceive('newQuery') // check for idIs scope
            ->once()
            ->andReturn($mockQuery);
        $mockQuery->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturn($modelResult);
        $modelResult->expects()->fill(['owner_id' => 2]);
        $modelResult->expects()->save()->andReturn(true);
        $result = $sut(Upsertable::fromModel($modelResult)->setKeyConverterClass(SnakeCaseKeyConvert::class));
        $this->assertEquals($modelResult, $result->model);
        $this->assertTrue($result->saved);
        $this->assertTrue($result->messages->isEmpty());
    }

    public function test_update_not_authorized(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 10]));
        $mockModel = m::mock(new ModelStub);
        UpsertGatePolicyStub::defineSelf($mockModel::class, $gate);
        EloquentUpsertAction::setGate($gate);
        $mockModel->shouldReceive('newInstance')->andReturnSelf();
        $mockQuery = m::mock(Builder::class);
        $mockQuery->allows()->getModel()->andReturn($mockModel);
        $sut = new class($mockModel) extends EloquentUpsertAction {};
        $modelResult = m::mock((new ModelStub)->forceFill(['id' => 1, 'owner_id' => 2]))->makePartial();
        $mockModel->shouldReceive('newQuery') // check for idIs scope
            ->once()
            ->andReturn($mockQuery);
        $mockQuery->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturn($modelResult);
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('You do not have permission to update this record.');
        $sut(Upsertable::fromModel($modelResult));
    }

    public function test_upsert_can_fail_validation(): void
    {
        $sut = new class(new ModelStub) extends EloquentUpsertAction
        {
            protected array $rules = ['foo' => 'required'];
        };
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('The foo field is required');
        $result = $sut(Upsertable::fromModel(new ModelStub));
    }

    public function test_upsert_can_fail_validation_with_rules_provider(): void
    {
        $sut = new class(new ModelStub) extends EloquentUpsertAction
        {
            protected string $rulesProvider = FooRules::class;
        };
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('The foo field is required');
        $result = $sut(Upsertable::fromModel(new ModelStub));
    }

    public function test_upsert_simple_with_missing_model_is_update(): void
    {
        $mockModel = m::mock(BaseEloquentModel::class);
        $mockModel->shouldReceive('newInstance')->with()->andReturnSelf();
        $mockQuery = m::mock(Builder::class);
        $mockQuery->allows()->getModel()->andReturn($mockModel);
        $sut = new class($mockModel) extends EloquentUpsertAction {};
        $mockModel->shouldReceive('newQuery') // check for idIs scope
            ->once()
            ->andReturn($mockQuery);
        $mockQuery->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturnNull();
        $modelResult = m::mock(new ModelStub)->makePartial();
        $mockModel->expects()->newInstance(['foo' => 'fooey', 'bar' => 'barrey'])
            ->andReturn($modelResult);
        $modelResult->expects()->save()->andReturn(true);
        $result = $sut(Upsertable::fromArray(['foo' => 'fooey', 'bar' => 'barrey'], 1));
        $this->assertEquals($modelResult, $result->model);
        $this->assertTrue($result->saved);
        $this->assertTrue($result->messages->isEmpty());
    }

    public function test_upsert_simple_with_model_can_create(): void
    {
        $mockModel = m::mock(BaseEloquentModel::class);
        $mockModel->shouldReceive('newInstance')->with()->andReturnSelf();
        $sut = new class($mockModel) extends EloquentUpsertAction {};
        $modelResult = m::mock((new ModelStub)->forceFill(['id' => 1, 'foo' => 'fooey', 'bar' => 'barrey']))
            ->makePartial();
        $mockModel->shouldReceive('newInstance')
            ->once()
            ->with(['foo' => 'fooey', 'bar' => 'barrey'])
            ->andReturn($modelResult);
        $modelResult->expects()->save()->andReturn(true);
        $result = $sut(Upsertable::fromArray(['foo' => 'fooey', 'bar' => 'barrey'], null));
        $this->assertEquals($modelResult, $result->model);
        $this->assertTrue($result->saved);
        $this->assertTrue($result->messages->isEmpty());
    }

    public function test_upsert_simple_with_model_can_update(): void
    {
        $mockModel = m::mock(BaseEloquentModel::class);
        $mockModel->shouldReceive('newInstance')->andReturnSelf();
        $mockQuery = m::mock(Builder::class);
        $mockQuery->allows()->getModel()->andReturn($mockModel);
        $sut = new class($mockModel) extends EloquentUpsertAction {};
        $modelResult = m::mock((new ModelStub)->forceFill(['id' => 1, 'foo' => 'fooey', 'bar' => 'barrey']))
            ->makePartial();
        $mockModel->shouldReceive('newQuery') // check for idIs scope
            ->once()
            ->andReturn($mockQuery);
        $mockQuery->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturn($modelResult);
        $modelResult->expects()->fill(['foo' => 'fooey', 'bar' => 'barrey']);
        $modelResult->expects()->save()->andReturn(true);
        $result = $sut(Upsertable::fromArray(['foo' => 'fooey', 'bar' => 'barrey'], 1));
        $this->assertEquals($modelResult, $result->model);
        $this->assertTrue($result->saved);
        $this->assertTrue($result->messages->isEmpty());
    }

    public function test_upsert_simple_with_model_wanting_identifier_can_create(): void
    {
        $mockModel = m::mock(BaseEloquentModel::class);
        $mockModel->shouldReceive('newInstance')->with()->andReturnSelf();
        $mockQuery = m::mock(Builder::class);
        $mockQuery->allows()->getModel()->andReturn($mockModel);
        $sut = new class($mockModel) extends EloquentUpsertAction
        {
            protected bool $includeIdentifierOnCreate = true;
        };
        $modelResult = m::mock((new ModelStub)->forceFill([
            'id' => 1, 'other_id' => 'i1', 'foo' => 'fooey', 'bar' => 'barrey',
        ]))
            ->makePartial();
        $mockModel->shouldReceive('newQuery') // check for idIs scope
            ->once()
            ->andReturn($mockQuery);
        $mockQuery->shouldReceive('find')
            ->once()
            ->with(['other_id' => 'i1'])
            ->andReturn(null);
        $mockModel->shouldReceive('newInstance')
            ->once()
            ->with(['foo' => 'fooey', 'bar' => 'barrey', 'other_id' => 'i1'])
            ->andReturn($modelResult);
        $modelResult->expects()->save()->andReturn(true);
        $result = $sut(Upsertable::fromArray(['foo' => 'fooey', 'bar' => 'barrey'], ['other_id' => 'i1']));
        $this->assertEquals($modelResult, $result->model);
        $this->assertTrue($result->saved);
        $this->assertTrue($result->messages->isEmpty());
    }

    protected function getGate(mixed $user): Gate
    {
        return new \Illuminate\Auth\Access\Gate(new Container, fn () => $user);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        EloquentUpsertAction::clearGate();
    }
}
