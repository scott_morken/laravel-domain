<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Unit\Actions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Auth\User;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Domain\Actions\Action;
use Smorken\Domain\Models\BaseEloquentModel;
use Smorken\Model\Contracts\Model;
use Tests\Smorken\Domain\Stubs\Policies\AuthorizeActionPolicy;
use Tests\Smorken\Domain\TestBenchTestCase;

class AuthorizeActionTest extends TestBenchTestCase
{
    #[Test]
    public function it_can_authorize_using_before_with_defined_abilities(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 1000]));

        AuthorizeActionPolicy::defineAbilities('AuthMe', $gate);
        $sut = $this->getSut();
        $sut::setGate($gate);
        $this->assertTrue($sut((new BaseEloquentModel)->forceFill(['user_id' => 10])));
    }

    #[Test]
    public function it_can_authorize_with_defined_abilities(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 99]));

        AuthorizeActionPolicy::defineAbilities('AuthMe', $gate);
        $sut = $this->getSut();
        $sut::setGate($gate);
        $model = (new BaseEloquentModel)->forceFill(['user_id' => 99]);
        $this->assertTrue($sut($model));
        $this->expectException(AuthorizationException::class);
        $sut((new BaseEloquentModel)->forceFill(['user_id' => 10]));
        $this->assertTrue($sut((new BaseEloquentModel)->forceFill(['user_id' => 1000])));
    }

    #[Test]
    public function it_can_skip_authorization(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 99]));

        AuthorizeActionPolicy::defineAbilities('AuthMe', $gate);
        $sut = $this->getSut();
        $sut::setGate($gate);
        $sut->setShouldAuthorize(false);
        $model = (new BaseEloquentModel)->forceFill(['user_id' => 99]);
        $this->assertTrue($sut($model));
        $this->assertTrue($sut((new BaseEloquentModel)->forceFill(['user_id' => 10])));
        $this->assertTrue($sut((new BaseEloquentModel)->forceFill(['user_id' => 1000])));
    }

    #[Test]
    public function it_can_throw_unauthorized_with_defined_abilities(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 99]));

        AuthorizeActionPolicy::defineAbilities('AuthMe', $gate);
        $sut = $this->getSut();
        $sut::setGate($gate);
        $this->expectException(AuthorizationException::class);
        $sut((new BaseEloquentModel)->forceFill(['user_id' => 10]));
    }

    protected function getGate(mixed $user): Gate
    {
        return new \Illuminate\Auth\Access\Gate(new Container, fn () => $user);
    }

    protected function getSut(): object
    {
        return new class extends Action
        {
            protected ?string $nameOfPolicy = 'AuthMe';

            public function __invoke(Model $model): bool
            {
                $this->authorize($model);

                return true;
            }
        };
    }
}
