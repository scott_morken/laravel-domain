<?php

namespace Tests\Smorken\Domain\Unit\Actions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Auth\User;
use Mockery as m;
use Smorken\Domain\Actions\EloquentDeleteAction;
use Smorken\Domain\Models\BaseEloquentModel;
use Tests\Smorken\Domain\Stubs\DataTransferObjects\GatePolicies\DeleteGatePolicyStub;
use Tests\Smorken\Domain\TestBenchTestCase;

class DeleteActionTest extends TestBenchTestCase
{
    public function test_delete_model_can_delete_using_authorization_when_before_matches(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 99]));
        $model = m::mock(BaseEloquentModel::class);
        DeleteGatePolicyStub::defineSelf($model::class, $gate);
        EloquentDeleteAction::setGate($gate);
        $model->shouldReceive('newInstance')
            ->andReturnSelf();
        $sut = new EloquentDeleteAction($model);
        $model->shouldReceive('getKey')
            ->once()
            ->andReturn(1);
        $model->shouldReceive('delete')
            ->once()
            ->andReturn(true);
        $result = $sut->delete($model);
        $this->assertEquals(1, $result->id);
        $this->assertTrue($result->deleted);
    }

    public function test_delete_model_can_delete_using_authorization_when_owner_matches(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 10]));
        $model = m::mock(BaseEloquentModel::class);
        DeleteGatePolicyStub::defineSelf($model::class, $gate);
        EloquentDeleteAction::setGate($gate);
        $model->shouldReceive('newInstance')
            ->andReturnSelf();
        $sut = new EloquentDeleteAction($model);
        $model->shouldReceive('getKey')
            ->once()
            ->andReturn(1);
        $model->expects()->getAttribute('owner_id')->andReturn(10);
        $model->shouldReceive('delete')
            ->once()
            ->andReturn(true);
        $result = $sut->delete($model);
        $this->assertEquals(1, $result->id);
        $this->assertTrue($result->deleted);
    }

    public function test_delete_model_cannot_delete_using_authorization(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 2]));
        $model = m::mock(BaseEloquentModel::class);
        DeleteGatePolicyStub::defineSelf($model::class, $gate);
        EloquentDeleteAction::setGate($gate);
        $model->shouldReceive('newInstance')
            ->andReturnSelf();
        $sut = new EloquentDeleteAction($model);
        $model->shouldReceive('getKey')
            ->once()
            ->andReturn(1);
        $model->expects()->getAttribute('owner_id')->andReturn(10);
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('You do not have permission to delete this record.');
        $sut->delete($model);
    }

    public function test_delete_model_successful(): void
    {
        $model = m::mock(BaseEloquentModel::class);
        $model->shouldReceive('newInstance')
            ->andReturnSelf();
        $sut = new EloquentDeleteAction($model);
        $model->shouldReceive('getKey')
            ->once()
            ->andReturn(1);
        $model->shouldReceive('delete')
            ->once()
            ->andReturn(true);
        $result = $sut->delete($model);
        $this->assertEquals(1, $result->id);
        $this->assertTrue($result->deleted);
    }

    protected function getGate(mixed $user): Gate
    {
        return new \Illuminate\Auth\Access\Gate(new Container, fn () => $user);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        EloquentDeleteAction::clearGate();
    }
}
