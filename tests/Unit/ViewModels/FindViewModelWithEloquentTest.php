<?php

namespace Tests\Smorken\Domain\Unit\ViewModels;

use Mockery as m;
use Smorken\Domain\ViewModels\RetrieveViewModel;
use Tests\Smorken\Domain\Concerns\WithCacheManager;
use Tests\Smorken\Domain\Stubs\Models\ModelStub;
use Tests\Smorken\Domain\TestBenchTestCase;

class FindViewModelWithEloquentTest extends TestBenchTestCase
{
    use WithCacheManager;

    public function test_model_instance_of_model(): void
    {
        $model = (new ModelStub)->forceFill(['id' => 1]);
        $sut = new class($model) extends RetrieveViewModel {};
        $this->assertEquals($model, $sut->model());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
