<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Unit\ViewModels\Concerns;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Domain\ViewModels\Concerns\WithSelectList;
use Smorken\Domain\ViewModels\IterableViewModel;

class WithSelectListTest extends TestCase
{
    #[Test]
    public function it_uses_defaults_to_create_select_list_for_model(): void
    {
        $model = new class(['id' => 1, 'name' => 'foo']) extends Model
        {
            protected $fillable = ['id', 'name'];
        };

        $collection = new Collection([$model]);
        $vm = new class($collection) extends IterableViewModel
        {
            use WithSelectList;
        };
        $this->assertEquals([
            1 => '{"id":1,"name":"foo"}',
        ], $vm->toSelectList());
    }

    #[Test]
    public function it_uses_defaults_to_create_select_list_for_array(): void
    {
        $model = ['id' => 1, 'name' => 'foo'];

        $collection = new Collection([$model]);
        $vm = new class($collection) extends IterableViewModel
        {
            use WithSelectList;
        };
        $this->assertEquals([
            1 => '{"id":1,"name":"foo"}',
        ], $vm->toSelectList());
    }

    #[Test]
    public function it_uses_label_attribute_to_create_select_list_for_model(): void
    {
        $model = new class(['id' => 1, 'name' => 'foo']) extends Model
        {
            protected $fillable = ['id', 'name'];
        };

        $collection = new Collection([$model]);
        $vm = new class($collection) extends IterableViewModel
        {
            use WithSelectList;
        };
        $vm->withLabel('name');
        $this->assertEquals([
            1 => 'foo',
        ], $vm->toSelectList());
    }

    #[Test]
    public function it_uses_label_attribute_to_create_select_list_for_array(): void
    {
        $model = ['id' => 1, 'name' => 'foo'];

        $collection = new Collection([$model]);
        $vm = new class($collection) extends IterableViewModel
        {
            use WithSelectList;
        };
        $vm->withLabel('name');
        $this->assertEquals([
            1 => 'foo',
        ], $vm->toSelectList());
    }

    #[Test]
    public function it_uses_closures_to_create_select_list_for_model(): void
    {
        $model = new class(['id1' => 1, 'id2' => 'bar', 'name' => 'foo', 'other' => 'biz']) extends Model
        {
            protected $fillable = ['id1', 'id2', 'name', 'other'];
        };

        $collection = new Collection([$model]);
        $vm = new class($collection) extends IterableViewModel
        {
            use WithSelectList;
        };
        $vm->withId(fn ($model) => $model->id1.$model->id2)
            ->withLabel(fn ($model) => $model->name.$model->other);
        $this->assertEquals([
            '1bar' => 'foobiz',
        ], $vm->toSelectList());
    }

    #[Test]
    public function it_can_set_a_default_value(): void
    {
        $model = ['id' => 1, 'name' => 'foo'];

        $collection = new Collection([$model]);
        $vm = new class($collection) extends IterableViewModel
        {
            use WithSelectList;
        };
        $vm->withLabel('name')
            ->withDefaultSelected(1);
        $this->assertEquals(1, $vm->defaultSelected());
    }
}
