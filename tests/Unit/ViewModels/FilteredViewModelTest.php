<?php

namespace Tests\Smorken\Domain\Unit\ViewModels;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Mockery as m;
use Smorken\Domain\ViewModels\FilteredViewModel;
use Smorken\Support\Filter;
use Tests\Smorken\Domain\TestBenchTestCase;

class FilteredViewModelTest extends TestBenchTestCase
{
    public function test_models_collection(): void
    {
        $filter = new Filter(['foo' => 'bar']);
        $models = new Collection;
        $sut = new class($filter, $models) extends FilteredViewModel {};
        $this->assertInstanceOf(Collection::class, $sut->models());
    }

    public function test_models_paginated(): void
    {
        $filter = new Filter(['foo' => 'bar']);
        $paginated = new \Illuminate\Pagination\Paginator([], 20);
        $sut = new class($filter, $paginated) extends FilteredViewModel {};
        $this->assertInstanceOf(Paginator::class, $sut->models());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
