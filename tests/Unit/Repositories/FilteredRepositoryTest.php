<?php

namespace Tests\Smorken\Domain\Unit\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Domain\Repositories\EloquentFilteredRepository;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Filter;
use Tests\Smorken\Domain\Concerns\WithCacheManager;

class FilteredRepositoryTest extends TestCase
{
    use WithCacheManager;

    public function test_collection(): void
    {
        $model = m::mock(Model::class);
        $sut = new EloquentFilteredRepository($model);
        $filter = new Filter;
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('filter')
            ->once()
            ->with($filter)
            ->andReturnSelf();
        $collected = new Collection;
        $query->shouldReceive('get')
            ->once()
            ->with(['*'])
            ->andReturn($collected);
        $results = $sut($filter, 0);
        $this->assertInstanceOf(Collection::class, $results);
        $this->assertSame($collected, $results);
    }

    public function test_modify_filter(): void
    {
        $model = m::mock(Model::class);
        $sut = new class($model) extends EloquentFilteredRepository
        {
            protected function modifyFilter(QueryStringFilter|\Smorken\Support\Contracts\Filter $filter
            ): \Smorken\Support\Contracts\Filter|QueryStringFilter {
                $filter->setAttribute('foo', 'bar');

                return $filter;
            }
        };
        $filter = new Filter;
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('filter')
            ->once()
            ->with($filter)
            ->andReturnSelf();
        $paginated = m::mock(Paginator::class);
        $query->shouldReceive('paginate')
            ->once()
            ->with(20, ['*'], 'page')
            ->andReturn($paginated);
        $sut($filter);
        $this->assertEquals('bar', $filter->foo);
    }

    public function test_paginated(): void
    {
        $model = m::mock(Model::class);
        $sut = new EloquentFilteredRepository($model);
        $filter = new Filter;
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('filter')
            ->once()
            ->with($filter)
            ->andReturnSelf();
        $paginated = m::mock(Paginator::class);
        $query->shouldReceive('paginate')
            ->once()
            ->with(20, ['*'], 'page')
            ->andReturn($paginated);
        $results = $sut($filter);
        $this->assertInstanceOf(Paginator::class, $results);
        $this->assertSame($paginated, $results);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
