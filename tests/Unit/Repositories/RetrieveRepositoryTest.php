<?php

namespace Tests\Smorken\Domain\Unit\Repositories;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\Model\Eloquent;
use Tests\Smorken\Domain\Concerns\WithCacheManager;
use Tests\Smorken\Domain\Stubs\DataTransferObjects\GatePolicies\RetrieveGatePolicyStub;
use Tests\Smorken\Domain\Stubs\Models\ModelStub;
use Tests\Smorken\Domain\Stubs\Models\RetrieveModelStub;

class RetrieveRepositoryTest extends TestCase
{
    use WithCacheManager;

    public function test_can_find_model(): void
    {
        $model = m::mock(Model::class);
        $sut = new EloquentRetrieveRepository($model);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('select')
            ->once()
            ->with(['*'])
            ->andReturnSelf();
        $eloquentModel = new class extends Eloquent {};
        $query->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturn($eloquentModel);
        $m = $sut(1, false);
        $this->assertSame($eloquentModel, $m);
    }

    public function test_can_find_or_fail_model(): void
    {
        $model = m::mock(Model::class);
        $sut = new EloquentRetrieveRepository($model);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('select')
            ->once()
            ->with(['*'])
            ->andReturnSelf();
        $eloquentModel = new class extends Eloquent {};
        $query->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($eloquentModel);
        $m = $sut(1);
        $this->assertSame($eloquentModel, $m);
    }

    public function test_it_can_create_empty_model_from_eloquent_model(): void
    {
        $model = new RetrieveModelStub;
        $sut = new EloquentRetrieveRepository($model);
        $empty = $sut->emptyModel();
        $this->assertInstanceOf(RetrieveModelStub::class, $empty);
        $this->assertEquals([], $empty->toArray());
    }

    public function test_it_can_fail_authorization_check(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 10]));
        $model = m::mock(new ModelStub);
        RetrieveGatePolicyStub::defineSelf($model::class, $gate);
        EloquentRetrieveRepository::setGate($gate);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('select')
            ->once()
            ->with(['*'])
            ->andReturnSelf();
        $foundModel = m::mock((new ModelStub)->forceFill(['id' => 1, 'owner_id' => 55]))->makePartial();
        $query->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($foundModel);
        $sut = new EloquentRetrieveRepository($model);
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('You do not have permission to view this record.');
        $m = $sut(1);
    }

    public function test_it_can_pass_authorization_check(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 10]));
        $model = m::mock(new ModelStub);
        RetrieveGatePolicyStub::defineSelf($model::class, $gate);
        EloquentRetrieveRepository::setGate($gate);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('select')
            ->once()
            ->with(['*'])
            ->andReturnSelf();
        $foundModel = m::mock((new ModelStub)->forceFill(['id' => 1, 'owner_id' => 10]))->makePartial();
        $query->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($foundModel);
        $sut = new EloquentRetrieveRepository($model);
        $m = $sut(1);
        $this->assertSame($foundModel, $m);
    }

    public function test_it_can_skip_authorization_check(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 10]));
        $model = m::mock(new ModelStub);
        RetrieveGatePolicyStub::defineSelf($model::class, $gate);
        EloquentRetrieveRepository::setGate($gate);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('select')
            ->once()
            ->with(['*'])
            ->andReturnSelf();
        $foundModel = m::mock((new ModelStub)->forceFill(['id' => 1, 'owner_id' => 55]))->makePartial();
        $query->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($foundModel);
        $sut = new EloquentRetrieveRepository($model);
        $sut->setShouldAuthorize(false);
        $m = $sut(1);
        $this->assertSame($foundModel, $m);
    }

    protected function getGate(mixed $user): Gate
    {
        return new \Illuminate\Auth\Access\Gate(new Container, fn () => $user);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        EloquentRetrieveRepository::clearGate();
    }
}
