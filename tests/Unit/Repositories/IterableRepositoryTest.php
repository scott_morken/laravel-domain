<?php

namespace Tests\Smorken\Domain\Unit\Repositories;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Domain\Repositories\EloquentIterableRepository;
use Tests\Smorken\Domain\Concerns\WithCacheManager;
use Tests\Smorken\Domain\Stubs\DataTransferObjects\GatePolicies\RetrieveGatePolicyStub;
use Tests\Smorken\Domain\Stubs\Models\ModelStub;

class IterableRepositoryTest extends TestCase
{
    use WithCacheManager;

    public function test_collection(): void
    {
        $model = m::mock(Model::class);
        $sut = new EloquentIterableRepository($model);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $collected = new Collection;
        $query->shouldReceive('get')
            ->once()
            ->with(['*'])
            ->andReturn($collected);
        $results = $sut(0);
        $this->assertInstanceOf(Collection::class, $results);
        $this->assertSame($collected, $results);
    }

    public function test_paginated(): void
    {
        $model = m::mock(Model::class);
        $sut = new EloquentIterableRepository($model);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $paginated = m::mock(Paginator::class);
        $query->shouldReceive('paginate')
            ->once()
            ->with(20, ['*'], 'page')
            ->andReturn($paginated);
        $results = $sut();
        $this->assertInstanceOf(Paginator::class, $results);
        $this->assertSame($paginated, $results);
    }

    public function test_it_can_fail_authorization_check(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 10]));
        $model = m::mock(new ModelStub);
        RetrieveGatePolicyStub::defineSelf($model::class, $gate);
        EloquentIterableRepository::setGate($gate);
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('You do not have permission to view these records.');
        $sut = new EloquentIterableRepository($model);
        $sut();
    }

    public function test_it_can_pass_authorization_check(): void
    {
        $gate = $this->getGate((new User)->forceFill(['id' => 99]));
        $model = m::mock(new ModelStub);
        RetrieveGatePolicyStub::defineSelf($model::class, $gate);
        EloquentIterableRepository::setGate($gate);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $paginated = m::mock(Paginator::class);
        $query->shouldReceive('paginate')
            ->once()
            ->with(20, ['*'], 'page')
            ->andReturn($paginated);
        $sut = new EloquentIterableRepository($model);
        $results = $sut();
        $this->assertInstanceOf(Paginator::class, $results);
        $this->assertSame($paginated, $results);
    }

    protected function getGate(mixed $user): Gate
    {
        return new \Illuminate\Auth\Access\Gate(new Container, fn () => $user);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        EloquentIterableRepository::clearGate();
    }
}
