<?php

namespace Tests\Smorken\Domain\Unit\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\Model\Eloquent;
use Tests\Smorken\Domain\Concerns\WithCacheManager;

class RetrieveRepositoryCachingTest extends TestCase
{
    use WithCacheManager;

    public function test_can_find_model_with_caching(): void
    {
        $model = m::mock(Model::class);
        $sut = new EloquentRetrieveRepository($model);
        $this->getCacheManager()
            ->shouldReceive('has')
            ->once()
            ->withArgs(function ($args) {
                $this->assertStringEndsWith('/1', $args);

                return true;
            })
            ->andReturn(true);
        $eloquentModel = new class extends Eloquent {};
        $this->getCacheManager()
            ->shouldReceive('get')
            ->once()
            ->with(m::type('string'))
            ->andReturn($eloquentModel);
        $m = $sut(1, false);
        $this->assertSame($eloquentModel, $m);
    }

    public function test_can_find_model_with_caching_disabled(): void
    {
        $model = m::mock(Model::class);
        $sut = new EloquentRetrieveRepository($model);
        $this->getCacheManager()
            ->shouldReceive('has')
            ->never();
        $this->getCacheManager()
            ->shouldReceive('get')
            ->never();
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->shouldReceive('newQuery')
            ->once()
            ->andReturn($query);
        $query->shouldReceive('select')
            ->once()
            ->with(['*'])
            ->andReturnSelf();
        $eloquentModel = new class extends Eloquent {};
        $query->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturn($eloquentModel);
        $sut->useCache(false);
        $m = $sut(1, false);
        $this->assertSame($eloquentModel, $m);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(false);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
