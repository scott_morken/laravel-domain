<?php

namespace Tests\Smorken\Domain\Unit\Factories;

use Illuminate\Container\Container;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\Domain\Repositories\Concerns\RepositoryGetsFilteredFromEloquent;
use Smorken\Domain\Repositories\Concerns\RepositoryGetsIterableFromEloquent;
use Smorken\Domain\Repositories\Concerns\RepositoryRetrievesFromEloquent;
use Smorken\Domain\Repositories\FilteredRepository;
use Smorken\Domain\Repositories\IterableRepository;
use Smorken\Domain\Repositories\Repository;
use Smorken\Domain\Repositories\RetrieveRepository;
use Smorken\Model\Contracts\Model;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Filter;
use Tests\Smorken\Domain\Concerns\WithCacheManager;
use Tests\Smorken\Domain\Stubs\Repositories\RepositoryContractStub;
use Tests\Smorken\Domain\Stubs\Repositories\RepositoryStub;

class RepositoryFactoryTest extends TestCase
{
    use WithCacheManager;

    protected ?Application $application = null;

    public function test_filtered(): void
    {
        $model = m::mock(\Illuminate\Database\Eloquent\Model::class);
        $repo = new class($model) extends FilteredRepository
        {
            use RepositoryGetsFilteredFromEloquent;

            public function __construct(protected \Illuminate\Database\Eloquent\Model $model) {}
        };
        $sut = new class([\Smorken\Domain\Repositories\Contracts\FilteredRepository::class => $repo]) extends RepositoryFactory
        {
            protected array $handlers = ['filtered' => \Smorken\Domain\Repositories\Contracts\FilteredRepository::class];

            public function filtered(
                QueryStringFilter|\Smorken\Support\Contracts\Filter $filter,
                int $perPage = 20
            ): \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Support\Collection|iterable {
                return $this->handlerForFiltered($filter, $perPage);
            }
        };
        $filter = new Filter;
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->expects()->newQuery()->once()->andReturn($query);
        $query->expects()->filter($filter)->once()->andReturnSelf();
        $paginator = new Paginator([], 20);
        $query->expects()->paginate(20, ['*'], 'page')->once()->andReturn($paginator);
        $m = $sut->filtered($filter);
        $this->assertSame($paginator, $m);
    }

    public function test_find(): void
    {
        $model = m::mock(\Illuminate\Database\Eloquent\Model::class);
        $repo = new class($model) extends RetrieveRepository
        {
            use RepositoryRetrievesFromEloquent;

            public function __construct(protected \Illuminate\Database\Eloquent\Model $model) {}
        };
        $sut = new class([\Smorken\Domain\Repositories\Contracts\RetrieveRepository::class => $repo]) extends RepositoryFactory
        {
            protected array $handlers = ['find' => \Smorken\Domain\Repositories\Contracts\RetrieveRepository::class];

            public function find(int $id, bool $throw = true): \Illuminate\Database\Eloquent\Model
            {
                return $this->handlerForFind($id, $throw);
            }
        };
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->expects()->newQuery()->once()->andReturn($query);
        $query->expects()->select(['*'])->once()->andReturnSelf();
        $resultModel = new class extends \Illuminate\Database\Eloquent\Model {};
        $query->expects()->findOrFail(1)->once()->andReturn($resultModel);
        $m = $sut->find(1);
        $this->assertSame($resultModel, $m);
    }

    public function test_for_filtered(): void
    {
        $model = m::mock(\Illuminate\Database\Eloquent\Model::class);
        $repo = new class($model) extends FilteredRepository
        {
            use RepositoryGetsFilteredFromEloquent;

            public function __construct(protected \Illuminate\Database\Eloquent\Model $model) {}
        };
        $sut = $this->getSut([
            \Smorken\Domain\Repositories\Contracts\FilteredRepository::class => $repo,
        ]);
        $filter = new Filter;
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->expects()->newQuery()->once()->andReturn($query);
        $query->expects()->filter($filter)->once()->andReturnSelf();
        $paginator = new Paginator([], 20);
        $query->expects()->paginate(20, ['*'], 'page')->once()->andReturn($paginator);
        $m = $sut->forFiltered(\Smorken\Domain\Repositories\Contracts\FilteredRepository::class, $filter);
        $this->assertSame($paginator, $m);
    }

    public function test_for_iterable(): void
    {
        $model = m::mock(\Illuminate\Database\Eloquent\Model::class);
        $repo = new class($model) extends IterableRepository
        {
            use RepositoryGetsIterableFromEloquent;

            public function __construct(protected \Illuminate\Database\Eloquent\Model $model) {}
        };
        $sut = $this->getSut([
            \Smorken\Domain\Repositories\Contracts\IterableRepository::class => $repo,
        ]);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->expects()->newQuery()->once()->andReturn($query);
        $paginator = new Paginator([], 20);
        $query->expects()->paginate(20, ['*'], 'page')->once()->andReturn($paginator);
        $m = $sut->forIterable(\Smorken\Domain\Repositories\Contracts\IterableRepository::class);
        $this->assertSame($paginator, $m);
    }

    public function test_for_retrieve(): void
    {
        $model = m::mock(\Illuminate\Database\Eloquent\Model::class);
        $repo = new class($model) extends RetrieveRepository
        {
            use RepositoryRetrievesFromEloquent;

            public function __construct(protected \Illuminate\Database\Eloquent\Model $model) {}
        };
        $sut = $this->getSut([
            \Smorken\Domain\Repositories\Contracts\RetrieveRepository::class => $repo,
        ]);
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturn($model);
        $model->expects()->newQuery()->once()->andReturn($query);
        $query->expects()->select(['*'])->once()->andReturnSelf();
        $resultModel = new class extends \Illuminate\Database\Eloquent\Model {};
        $query->expects()->findOrFail(1)->once()->andReturn($resultModel);
        $m = $sut->forRetrieve(\Smorken\Domain\Repositories\Contracts\RetrieveRepository::class, 1);
        $this->assertSame($resultModel, $m);
    }

    public function test_for_with_invoke_args(): void
    {
        $repo = new RepositoryStub;
        $sut = $this->getSut([
            RepositoryContractStub::class => $repo,
        ]);
        $this->assertEquals(['foo' => 'foo', 'bar' => 'bar'],
            $sut->for(RepositoryContractStub::class, ['foo' => 'foo', 'bar' => 'bar']));
    }

    public function test_for_without_invoke_args(): void
    {
        $repo = m::mock(RepositoryStub::class);
        $sut = $this->getSut([
            RepositoryContractStub::class => $repo,
        ]);
        $repo->allows()->__invoke()->andReturns('result');
        $this->assertEquals('result', $sut->for(RepositoryContractStub::class));
    }

    public function test_make_using_container(): void
    {
        $sut = $this->getSut([
            RepositoryContractStub::class => RepositoryStub::class,
        ]);
        $repo = m::mock(RepositoryStub::class);
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(RepositoryStub::class, [])
            ->andReturn($repo);
        $this->assertSame($repo, $sut->make(RepositoryContractStub::class));
    }

    public function test_make_with_instantiated_class(): void
    {
        $model = m::mock(Model::class);
        $repo = new class($model) extends Repository {};
        $sut = $this->getSut([
            \Smorken\Domain\Repositories\Contracts\Repository::class => $repo,
        ]);
        $this->assertSame($repo, $sut->make(\Smorken\Domain\Repositories\Contracts\Repository::class));
    }

    public function test_for_retrieve_with_mock_repo(): void
    {
        $repo = m::mock(\Smorken\Domain\Repositories\Contracts\RetrieveRepository::class);
        $sut = $this->getSut([
            \Smorken\Domain\Repositories\Contracts\RetrieveRepository::class => $repo,
        ]);
        $resultModel = new class extends \Illuminate\Database\Eloquent\Model {};
        $repo->shouldReceive('__invoke')
            ->once()
            ->with(1, true)
            ->andReturn($resultModel);
        $m = $sut->forRetrieve(\Smorken\Domain\Repositories\Contracts\RetrieveRepository::class, 1);
        $this->assertSame($resultModel, $m);
    }

    protected function getApplication(): Application
    {
        if (! $this->application) {
            $this->application = m::mock(Application::class);
        }

        return $this->application;
    }

    protected function getSut(array $repositories): RepositoryFactory
    {
        RepositoryFactory::setApplication($this->getApplication());

        return new RepositoryFactory($repositories);
    }

    protected function initHttpRequest(): void
    {
        $container = Container::getInstance();
        $container->bind('request', fn () => new Request);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initCacheAssist(true);
        $this->initHttpRequest();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
