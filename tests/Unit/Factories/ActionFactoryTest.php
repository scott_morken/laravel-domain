<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Unit\Factories;

use Illuminate\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Domain\Actions\Action;
use Smorken\Domain\Actions\Contracts\DeleteAction;
use Smorken\Domain\Actions\Contracts\UpsertAction;
use Smorken\Domain\Actions\EloquentDeleteAction;
use Smorken\Domain\Actions\EloquentUpsertAction;
use Smorken\Domain\Actions\Results\DeleteResult;
use Smorken\Domain\Actions\Results\SaveResult;
use Smorken\Domain\Actions\Upsertable;
use Smorken\Domain\Factories\ActionFactory;
use Smorken\Model\Contracts\Model;
use Tests\Smorken\Domain\Stubs\Repositories\ActionStub;

class ActionFactoryTest extends TestCase
{
    protected ?Application $application = null;

    public function test_for_can_invoke_with_args(): void
    {
        $action = new ActionStub;
        $sut = $this->getSut([
            \Smorken\Domain\Actions\Contracts\Action::class => $action,
        ]);
        $this->assertEquals(['foo' => 'foo', 'bar' => 'bar'],
            $sut->for(\Smorken\Domain\Actions\Contracts\Action::class, ['foo' => 'foo', 'bar' => 'bar']));
    }

    public function test_for_can_invoke_without_args(): void
    {
        $action = new ActionStub;
        $sut = $this->getSut([
            \Smorken\Domain\Actions\Contracts\Action::class => $action,
        ]);
        $this->assertEquals('result', $sut->for(\Smorken\Domain\Actions\Contracts\Action::class));
    }

    public function test_for_delete(): void
    {
        $deleteAction = m::mock(EloquentDeleteAction::class);
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(EloquentDeleteAction::class, [])
            ->andReturn($deleteAction);
        $deleteResult = new DeleteResult(1, true, new MessageBag);
        $deleteAction->shouldReceive('__invoke')
            ->once()
            ->with(1, true)
            ->andReturn($deleteResult);
        $sut = $this->getSut([
            DeleteAction::class => EloquentDeleteAction::class,
        ]);
        $result = $sut->forDelete(DeleteAction::class, 1);
        $this->assertSame($deleteResult, $result);
    }

    public function test_for_upsert(): void
    {
        $upsertAction = m::mock(EloquentUpsertAction::class);
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(EloquentUpsertAction::class, [])
            ->andReturn($upsertAction);
        $saveResult = new SaveResult(m::mock(Model::class), true, true, new MessageBag);
        $upsertAction->shouldReceive('__invoke')
            ->once()
            ->with(m::type(\Smorken\Domain\Actions\Contracts\Upsertable::class))
            ->andReturn($saveResult);
        $sut = $this->getSut([
            UpsertAction::class => EloquentUpsertAction::class,
        ]);
        $this->assertSame($saveResult, $sut->forUpsert(UpsertAction::class, Upsertable::fromArray(['foo' => 'bar'], null)));
    }

    public function test_it_can_make_using_the_container(): void
    {
        $action = new ActionStub;
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(ActionStub::class, [])
            ->andReturn($action);
        $sut = $this->getSut([
            \Smorken\Domain\Actions\Contracts\Action::class => ActionStub::class,
        ]);
        $this->assertSame($action, $sut->make(\Smorken\Domain\Actions\Contracts\Action::class));
    }

    public function test_it_can_make_with_an_instantiated_action(): void
    {
        $action = new class extends Action {};
        $sut = $this->getSut([
            \Smorken\Domain\Actions\Contracts\Action::class => $action,
        ]);
        $this->assertSame($action, $sut->make(\Smorken\Domain\Actions\Contracts\Action::class));
    }

    protected function getApplication(): Application
    {
        if (! $this->application) {
            $this->application = m::mock(Application::class);
        }

        return $this->application;
    }

    protected function getSut(array $actions): ActionFactory
    {
        ActionFactory::setApplication($this->getApplication());

        return new ActionFactory($actions);
    }

    protected function initHttpRequest(): void
    {
        $container = Container::getInstance();
        $container->bind('request', fn () => new Request);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initHttpRequest();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
