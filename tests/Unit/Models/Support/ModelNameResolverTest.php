<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Unit\Models\Support;

use Database\Factories\Eloquent\Dummy\DummyModelFactory;
use Database\Factories\Eloquent\Dummy\SimpleModelFactory;
use Database\Factories\Smorken\Auth\Models\Eloquent\UserFactory;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Domain\Models\Support\ModelNameResolver;
use Smorken\Domain\Models\Support\NamespacesVO;

class ModelNameResolverTest extends TestCase
{
    #[Test]
    public function it_resolves_a_domain_eloquent_factory(): void
    {
        $ns = new NamespacesVO('Domain\\', 'Database\\Factories\\Eloquent\\', 'Database\\Factories\\');
        $fn = ModelNameResolver::resolver($ns);
        $this->assertEquals('Domain\Dummy\Models\Eloquent\DummyModel', $fn(new DummyModelFactory));
    }

    #[Test]
    public function it_resolves_a_domain_simple_factory(): void
    {
        $ns = new NamespacesVO('Domain\\', 'Database\\Factories\\Eloquent\\', 'Database\\Factories\\');
        $fn = ModelNameResolver::resolver($ns);
        $this->assertEquals('Domain\Dummy\Models\SimpleModel', $fn(new SimpleModelFactory));
    }

    #[Test]
    public function it_resolves_a_package_factory(): void
    {
        $ns = new NamespacesVO('Domain\\', 'Database\\Factories\\Eloquent\\', 'Database\\Factories\\');
        $fn = ModelNameResolver::resolver($ns);
        $this->assertEquals('Smorken\Auth\Models\Eloquent\User', $fn(new UserFactory));
    }
}
