<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Unit\Models\Support;

use Domain\Dummy\Models\Eloquent\DummyModel;
use Domain\Dummy\Models\SimpleModel;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\Domain\Models\Support\FactoryNameResolver;
use Smorken\Domain\Models\Support\NamespacesVO;

class FactoryNameResolverTest extends TestCase
{
    #[Test]
    public function it_should_resolve_an_domain_named_factory(): void
    {
        $ns = new NamespacesVO('Domain\\', 'Database\\Factories\\Eloquent\\', 'Database\\Factories\\');
        $fn = FactoryNameResolver::resolver($ns);
        $this->assertEquals('Database\Factories\Eloquent\Dummy\DummyModelFactory', $fn(DummyModel::class));
    }

    #[Test]
    public function it_should_resolve_a_simple_domain_named_factory(): void
    {
        $ns = new NamespacesVO('Domain\\', 'Database\\Factories\\Eloquent\\', 'Database\\Factories\\');
        $fn = FactoryNameResolver::resolver($ns);
        $this->assertEquals('Database\Factories\Eloquent\Dummy\SimpleModelFactory', $fn(SimpleModel::class));
    }

    #[Test]
    public function it_should_resolve_a_package_factory(): void
    {
        $ns = new NamespacesVO('Domain\\', 'Database\\Factories\\Eloquent\\', 'Database\\Factories\\');
        $fn = FactoryNameResolver::resolver($ns);
        $this->assertEquals('Database\Factories\Smorken\Auth\Models\Eloquent\UserFactory', $fn(User::class));
    }
}
