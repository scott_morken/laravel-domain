<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Unit\Cache;

use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\CacheAssist\Contracts\CacheAssist;
use Smorken\Domain\Cache\CacheHandler;

class CacheHandlerTest extends TestCase
{
    #[Test]
    public function it_retries_callback_by_default_when_cache_is_null(): void
    {
        $ca = m::mock(CacheAssist::class);
        $sut = new CacheHandler('test');
        $sut->setCacheAssist($ca);
        $ca->expects()->remember(['foo'], 20, m::type(\Closure::class))
            ->andReturns(null);
        $v = $sut->remember('foo', 20, fn () => 'bar');
        $this->assertEquals('bar', $v);
    }

    #[Test]
    public function it_returns_null_when_cache_is_null_and_retry_is_false(): void
    {
        $ca = m::mock(CacheAssist::class);
        $sut = new CacheHandler('test');
        $sut->setCacheAssist($ca);
        $ca->expects()->remember(['foo'], 20, m::type(\Closure::class))
            ->andReturns(null);
        $v = $sut->remember('foo', 20, fn () => 'bar', false);
        $this->assertEquals(null, $v);
    }

    #[Test]
    public function it_returns_value_from_cache_assist(): void
    {
        $ca = m::mock(CacheAssist::class);
        $sut = new CacheHandler('test');
        $sut->setCacheAssist($ca);
        $ca->expects()->remember(['foo'], 20, m::type(\Closure::class))
            ->andReturns('cached bar');
        $v = $sut->remember('foo', 20, fn () => 'bar');
        $this->assertEquals('cached bar', $v);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
