<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Factories;

use Smorken\Domain\Factories\ActionFactory;
use Tests\Smorken\Domain\Phpstan\Actions\EloquentDeleteActionStan;
use Tests\Smorken\Domain\Phpstan\Actions\EloquentUpsertActionStan;

/**
 * @extends ActionFactory<\Tests\Smorken\Domain\Phpstan\Actions\Stubs\EloquentStan>
 */
class ActionFactoryStan extends ActionFactory
{
    protected array $handlers = [
        'upsert' => EloquentUpsertActionStan::class,
        'delete' => EloquentDeleteActionStan::class,
    ];
}

$a = new ActionFactoryStan;
$made = $a->make(EloquentUpsertActionStan::class);
assert($made instanceof EloquentUpsertActionStan);
