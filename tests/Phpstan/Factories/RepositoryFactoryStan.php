<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Factories;

use Illuminate\Support\Collection;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Domain\Phpstan\Repositories\EloquentFilteredRepositoryStan;
use Tests\Smorken\Domain\Phpstan\Repositories\EloquentRetrieveRepositoryStan;
use Tests\Smorken\Domain\Phpstan\Repositories\Stubs\EloquentStan;

/**
 * @extends RepositoryFactory<EloquentStan>
 */
class RepositoryFactoryStan extends RepositoryFactory
{
    protected array $handlers = [
        'find' => EloquentRetrieveRepositoryStan::class,
        'filtered' => EloquentFilteredRepositoryStan::class,
    ];

    public function all(): iterable|Collection
    {
        return $this->handlerForAll();
    }

    public function emptyModel(): EloquentStan
    {
        return $this->handlerForEmptyModel();
    }

    public function filtered(
        QueryStringFilter|Filter $filter,
        int $perPage = 20
    ): \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Support\Collection|iterable {
        return $this->handlerForFiltered($filter, $perPage);
    }

    public function find(mixed $id, bool $throw = true): ?EloquentStan
    {
        return $this->handlerForFind($id, $throw);
    }
}

$f = new RepositoryFactoryStan;
assert($f->emptyModel() instanceof EloquentStan);
