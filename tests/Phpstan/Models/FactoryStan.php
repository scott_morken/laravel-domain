<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Models;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<EloquentStan>
 */
class FactoryStan extends Factory
{
    protected $model = EloquentStan::class;

    public function definition(): array
    {
        return [];
    }
}
