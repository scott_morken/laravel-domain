<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Smorken\Domain\Models\BaseEloquentModel;

class EloquentStan extends BaseEloquentModel
{
    /** @use \Illuminate\Database\Eloquent\Factories\HasFactory<\Tests\Smorken\Domain\Phpstan\Models\FactoryStan> */
    use HasFactory;
}

$f = EloquentStan::factory();
assert($f instanceof FactoryStan);
