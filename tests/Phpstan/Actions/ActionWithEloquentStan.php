<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Actions;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\Model\Contracts\Model;
use Tests\Smorken\Domain\Phpstan\Actions\Stubs\EloquentBuilderStan;
use Tests\Smorken\Domain\Phpstan\Actions\Stubs\EloquentStan;

/**
 * @extends ActionWithEloquent<EloquentStan>
 */
class ActionWithEloquentStan extends ActionWithEloquent
{
    public function __invoke(): Model|EloquentModel
    {
        return $this->eloquentModel();
    }
}

$a = new ActionWithEloquentStan(new EloquentStan);
assert($a instanceof ActionWithEloquentStan);
$m = $a();
assert($m instanceof EloquentStan);
assert($m->foo() instanceof EloquentStan);
assert($m->newQuery()->bar() instanceof EloquentBuilderStan);
