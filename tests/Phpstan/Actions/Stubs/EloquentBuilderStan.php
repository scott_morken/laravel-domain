<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Actions\Stubs;

use Smorken\Model\QueryBuilders\Builder;

/**
 * @template TModel of \Smorken\Model\Eloquent
 *
 * @extends Builder<TModel>
 */
class EloquentBuilderStan extends Builder
{
    /**
     * @return $this<TModel>
     */
    public function bar(): self
    {
        return $this;
    }
}
