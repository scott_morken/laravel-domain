<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Actions\Stubs;

use Illuminate\Database\Eloquent\HasBuilder;
use Smorken\Domain\Models\BaseEloquentModel;

class EloquentStan extends BaseEloquentModel
{
    /** @use HasBuilder<\Tests\Smorken\Domain\Phpstan\Actions\Stubs\EloquentBuilderStan<static>> */
    use HasBuilder;

    protected static string $builder = EloquentBuilderStan::class;

    public function foo(): self
    {
        return $this;
    }
}
