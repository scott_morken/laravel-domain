<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Actions\Stubs;

use Smorken\Domain\Actions\Upsertable;

class UpsertableStan extends Upsertable
{
    public function fizz(): void
    {
        // do nothing
    }
}
