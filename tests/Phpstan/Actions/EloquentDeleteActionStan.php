<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Actions;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Domain\Actions\EloquentDeleteAction;
use Smorken\Model\Contracts\Model;
use Tests\Smorken\Domain\Phpstan\Actions\Stubs\EloquentStan;

/**
 * @extends EloquentDeleteAction<EloquentStan>
 */
class EloquentDeleteActionStan extends EloquentDeleteAction
{
    public function model(): Model|EloquentModel
    {
        return $this->eloquentModel();
    }
}

$a = new EloquentDeleteAction(new EloquentStan);
assert($a instanceof EloquentDeleteAction);

$a = new EloquentDeleteActionStan(new EloquentStan);
assert($a instanceof EloquentDeleteActionStan);

assert($a->model() instanceof EloquentStan);
