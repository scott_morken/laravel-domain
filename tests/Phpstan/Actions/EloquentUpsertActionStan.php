<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Actions;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Domain\Actions\Contracts\Upsertable as UpsertableContract;
use Smorken\Domain\Actions\EloquentUpsertAction;
use Smorken\Model\Contracts\Model;
use Smorken\Model\Contracts\Model as ModelContract;
use Tests\Smorken\Domain\Phpstan\Actions\Stubs\EloquentStan;
use Tests\Smorken\Domain\Phpstan\Actions\Stubs\UpsertableStan;

/**
 * @template TModel of ModelContract|EloquentModel
 *
 * @extends EloquentUpsertAction<TModel, UpsertableStan>
 */
class EloquentUpsertActionStan extends EloquentUpsertAction
{
    /**
     * @return TModel
     */
    public function model(): Model|EloquentModel
    {
        return $this->eloquentModel();
    }

    protected function upsertModelFromUpsertable(UpsertableContract $upsertable
    ): \Illuminate\Database\Eloquent\Model|ModelContract {
        $upsertable->fizz();

        return $this->model();
    }
}

$a = new EloquentUpsertActionStan(new EloquentStan);
assert($a instanceof EloquentUpsertActionStan);

assert($a->model() instanceof EloquentStan);

$u = new UpsertableStan([], null);

assert($a($u) instanceof EloquentStan);
