<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\EloquentFilteredRepository;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Domain\Phpstan\Repositories\Stubs\EloquentStan;

/**
 * @extends \Smorken\Domain\Repositories\EloquentFilteredRepository<EloquentStan>
 */
class EloquentFilteredRepositoryStan extends EloquentFilteredRepository
{
    protected function getFiltered(Filter|QueryStringFilter $filter, int $perPage): iterable|Paginator|Collection
    {
        if ($perPage) {
            return $this->modifyPaginator(new \Illuminate\Pagination\Paginator(new Collection([$this->model()]),
                $perPage));
        }

        return $this->modifyCollection(new Collection([$this->model()]));
    }
}

$r = new EloquentFilteredRepositoryStan(new EloquentStan(['foo' => 'bar']));
$f = new \Smorken\QueryStringFilter\QueryStringFilter(new Request);

assert($r->model()->foo() instanceof EloquentStan);
assert($r($f) instanceof Paginator);
assert($r($f, 0) instanceof Collection);
