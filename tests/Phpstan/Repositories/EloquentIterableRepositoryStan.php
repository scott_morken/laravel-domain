<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\EloquentIterableRepository;
use Tests\Smorken\Domain\Phpstan\Repositories\Stubs\EloquentStan;

/**
 * @extends EloquentIterableRepository<EloquentStan>
 */
class EloquentIterableRepositoryStan extends EloquentIterableRepository
{
    protected function getIterable(int $perPage): iterable|Paginator|Collection
    {
        if ($perPage) {
            return $this->modifyPaginator(new \Illuminate\Pagination\Paginator(new Collection([$this->model()]),
                $perPage));
        }

        return $this->modifyCollection(new Collection([$this->model()]));
    }
}

$r = new EloquentIterableRepositoryStan(new EloquentStan(['foo' => 'bar']));

assert($r->model()->foo() instanceof EloquentStan);
assert($r() instanceof Paginator);
assert($r(0) instanceof Collection);
