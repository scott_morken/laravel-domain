<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\FilteredRepository;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Domain\Phpstan\Repositories\Stubs\VOStan;

/**
 * @extends FilteredRepository<VOStan>
 */
class FilteredRepositoryStan extends FilteredRepository
{
    protected function getFiltered(QueryStringFilter|Filter $filter, int $perPage): iterable|Paginator|Collection
    {
        return new Collection([new VOStan('bar')]);
    }
}

$r = new FilteredRepositoryStan;
$collection = $r(new \Smorken\QueryStringFilter\QueryStringFilter(new Request));
assert($collection instanceof Collection);
assert($collection->first() instanceof VOStan);
