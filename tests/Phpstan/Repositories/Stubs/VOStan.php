<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Repositories\Stubs;

interface VOStanInterface
{
    /**
     * @return array<string, string>
     */
    public function toArray(): array;
}

class VOStan implements VOStanInterface
{
    public function __construct(
        public string $foo
    ) {}

    public function toArray(): array
    {
        return [
            'foo' => $this->foo,
        ];
    }
}
