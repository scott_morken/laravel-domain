<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Repositories\Stubs;

use Illuminate\Database\Eloquent\HasBuilder;
use Smorken\Model\Eloquent;

class EloquentStan extends Eloquent
{
    /** @use HasBuilder<\Tests\Smorken\Domain\Phpstan\Repositories\Stubs\EloquentBuilderStan<static>> */
    use HasBuilder;

    protected static string $builder = EloquentBuilderStan::class;

    public function foo(): self
    {
        return $this;
    }
}
