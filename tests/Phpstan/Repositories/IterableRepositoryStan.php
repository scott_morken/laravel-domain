<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\IterableRepository;
use Tests\Smorken\Domain\Phpstan\Repositories\Stubs\VOStan;

/**
 * @extends IterableRepository<VOStan>
 */
class IterableRepositoryStan extends IterableRepository
{
    protected function getIterable(int $perPage): iterable|Paginator|Collection
    {
        return new Collection([new VOStan('bar')]);
    }
}

$r = new IterableRepositoryStan;
$collection = $r();
assert($collection instanceof Collection);
assert($collection->first() instanceof VOStan);
