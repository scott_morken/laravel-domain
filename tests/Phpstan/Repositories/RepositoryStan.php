<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Repositories;

use Smorken\Domain\Repositories\Repository;
use Tests\Smorken\Domain\Phpstan\Repositories\Stubs\VOStan;
use Tests\Smorken\Domain\Phpstan\Repositories\Stubs\VOStanInterface;

/**
 * @template TModel of VOStanInterface
 *
 * @extends Repository<VOStan>
 */
class RepositoryStan extends Repository
{
    /**
     * @param  TModel  $model
     */
    public function __construct(protected object $model) {}

    /**
     * @return TModel
     */
    public function __invoke(): VOStanInterface
    {
        return $this->model;
    }
}

$r = new RepositoryStan(new VOStan('bar'));

assert($r()->toArray() === ['foo' => 'bar']);
