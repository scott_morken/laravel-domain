<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Repositories;

use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Tests\Smorken\Domain\Phpstan\Repositories\Stubs\EloquentStan;

/**
 * @extends EloquentRetrieveRepository<EloquentStan>
 */
class EloquentRetrieveRepositoryStan extends EloquentRetrieveRepository {}

$r = new EloquentRetrieveRepositoryStan(new EloquentStan(['foo' => 'bar']));

assert($r->model()->foo() instanceof EloquentStan);
