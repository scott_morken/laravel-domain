<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Repositories;

use Smorken\Domain\Repositories\RetrieveRepository;
use Tests\Smorken\Domain\Phpstan\Repositories\Stubs\VOStan;

/**
 * @extends RetrieveRepository<\Tests\Smorken\Domain\Phpstan\Repositories\Stubs\VOStan>
 */
class RetrieveRepositoryStan extends RetrieveRepository
{
    public function __construct(protected object $model) {}

    protected function retrieve(mixed $id, bool $throw): mixed
    {
        return $this->model;
    }
}

$r = new RetrieveRepositoryStan(new VOStan('bar'));

assert($r(1)->foo === 'bar');
