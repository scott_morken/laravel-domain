<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Repositories;

use Illuminate\Support\LazyCollection;
use Smorken\Domain\Repositories\LazyCollectionRepository;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Domain\Phpstan\Repositories\Stubs\VOStan;

/**
 * @extends LazyCollectionRepository<VOStan>
 */
class LazyCollectionRepositoryStan extends LazyCollectionRepository
{
    protected function getLazyCollection(QueryStringFilter|Filter|null $filter): LazyCollection
    {
        return new LazyCollection([new VOStan('foo')]);
    }
}

$r = new LazyCollectionRepositoryStan;
$collection = $r();
assert($collection instanceof LazyCollection);
assert($collection->first() instanceof VOStan);
