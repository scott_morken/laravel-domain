<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Repositories;

use Illuminate\Support\LazyCollection;
use Smorken\Domain\Repositories\EloquentLazyCollectionRepository;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Domain\Phpstan\Repositories\Stubs\EloquentStan;

/**
 * @extends EloquentLazyCollectionRepository<EloquentStan>
 */
class EloquentLazyCollectionRepositoryStan extends EloquentLazyCollectionRepository
{
    protected function getLazyCollection(QueryStringFilter|Filter|null $filter): LazyCollection
    {
        return new LazyCollection([new EloquentStan(['foo' => 'bar'])]);
    }
}

$r = new EloquentIterableRepositoryStan(new EloquentStan(['foo' => 'bar']));

assert($r->model()->foo() instanceof EloquentStan);
assert($r() instanceof LazyCollection);
