<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Authorization\Stubs;

use Smorken\Model\Eloquent;

class EloquentStan extends Eloquent
{
    public function foo(): self
    {
        return $this;
    }
}
