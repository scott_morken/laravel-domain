<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\Authorization\Policies;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\Domain\Authorization\Policies\ModelBasedPolicy;
use Smorken\Model\Contracts\Model;
use Tests\Smorken\Domain\Phpstan\Authorization\Stubs\EloquentStan;

/**
 * @extends ModelBasedPolicy<User, \Tests\Smorken\Domain\Phpstan\Authorization\Stubs\EloquentStan>
 */
class ModelStanPolicyStan extends ModelBasedPolicy
{
    protected function isUserAllowedToView(Authenticatable $user, Model $model): Response
    {
        $model = $model->foo();

        return parent::isUserAllowedToView($user, $model);
    }
}

$p = new ModelStanPolicyStan;
$m = new EloquentStan;
assert($p->view(new User, $m) instanceof Response);
