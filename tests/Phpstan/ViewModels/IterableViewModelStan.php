<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\ViewModels;

use Illuminate\Support\Collection;
use Smorken\Domain\ViewModels\IterableViewModel;
use Tests\Smorken\Domain\Phpstan\Models\EloquentStan;

$vm = new IterableViewModel(new Collection([new EloquentStan]));
$models = $vm->models();

assert($models instanceof Collection);
assert($models->first() instanceof EloquentStan);
