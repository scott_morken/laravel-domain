<?php

declare(strict_types=1);

namespace Tests\Smorken\Domain\Phpstan\ViewModels;

use Smorken\Domain\ViewModels\RetrieveViewModel;
use Tests\Smorken\Domain\Phpstan\Models\EloquentStan;

$vm = new RetrieveViewModel(new EloquentStan);

assert($vm->model() instanceof EloquentStan);
