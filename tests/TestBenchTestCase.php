<?php

namespace Tests\Smorken\Domain;

use Orchestra\Testbench\TestCase;
use Smorken\Domain\ActionServiceProvider;
use Smorken\Domain\ModelServiceProvider;
use Smorken\Domain\RepositoryServiceProvider;

class TestBenchTestCase extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [
            ActionServiceProvider::class,
            ModelServiceProvider::class,
            RepositoryServiceProvider::class,
        ];
    }
}
