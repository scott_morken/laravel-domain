<?php

namespace Smorken\Domain\Builders\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;

trait HasActiveAttribute
{
    public function active(): Builder
    {
        return $this->scopeActiveIs($this, 1);
    }

    public function activeIs(int $active = 1): Builder
    {
        return $this->scopeActiveIs($this, $active);
    }

    public function scopeActive(Builder $query): Builder
    {
        return $this->scopeActiveIs($query, 1);
    }

    public function scopeActiveIs(Builder $query, int $active = 1): Builder
    {
        return $query->where($this->getActiveAttributeName(), '=', $active);
    }

    protected function getActiveAttributeName(): string
    {
        if (property_exists($this, 'activeAttributeName')) {
            return $this->activeAttributeName;
        }

        return 'active';
    }
}
