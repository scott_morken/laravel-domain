<?php

namespace Smorken\Domain;

use Illuminate\Support\ServiceProvider;
use Smorken\Domain\Support\Helpers\FilterHelper;
use Smorken\Sanitizer\Contracts\Sanitize;

class DomainServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if ($this->app->bound(Sanitize::class)) {
            FilterHelper::setSanitizer($this->app[Sanitize::class]);
        }
        $this->bootConfigs();
    }

    protected function bootConfigs(): void
    {
        $config = __DIR__.'/../config/domain.php';
        $this->mergeConfigFrom($config, 'sm-domain');
        $this->publishes([$config => config_path('sm-domain.php')], 'config');
    }
}
