<?php

declare(strict_types=1);

namespace Smorken\Domain\Authorization\Concerns;

use Illuminate\Contracts\Auth\Access\Gate;
use Smorken\Domain\Authorization\Constants\PolicyType;

trait Authorizes
{
    protected static ?Gate $gate = null;

    protected ?string $nameOfPolicy = null;

    protected bool $shouldAuthorize = true;

    protected bool $throwsAuthorizationException = true;

    public static function clearGate(): void
    {
        self::$gate = null;
    }

    public static function setGate(Gate $gate): void
    {
        self::$gate = $gate;
    }

    public function authorize(mixed $object = null, ?PolicyType $policyType = null): bool
    {
        if ($this->shouldAuthorize && $this->getGate() && $this->gateHasPolicyFor($this->getNameOfPolicy($object))) {
            $ability = $this->getGateAbility($object, $policyType);

            return $this->check($ability, $object);
        }

        return true;
    }

    public function check(string $gateName, mixed $args = null): bool
    {
        if ($this->throwsAuthorizationException) {
            return $this->getGate()->authorize($gateName, $args)->allowed();
        }

        return $this->getGate()->allows($gateName, $args);
    }

    public function gateHasPolicyFor(string $name): bool
    {
        $policies = $this->getGate()?->policies() ?? [];

        return isset($policies[$name]) || $this->gateHasAbilityFor($name);
    }

    public function getGate(): ?Gate
    {
        return self::$gate;
    }

    public function setShouldAuthorize(bool $authorize): self
    {
        $this->shouldAuthorize = $authorize;

        return $this;
    }

    public function setThrowsAuthorizationException(bool $throw): void
    {
        $this->throwsAuthorizationException = $throw;
    }

    protected function gateHasAbilityFor(string $name): bool
    {
        $abilities = $this->getGate()?->abilities() ?? [];
        foreach ($abilities as $ability => $c) {
            $parts = explode('.', (string) $ability);
            if ($parts[0] === $name) {
                return true;
            }
        }

        return false;
    }

    protected function getGateAbility(mixed $object, ?PolicyType $policyType): string
    {
        $type = $policyType ? $policyType->toMethodName() : $this->getGatePolicyType();

        return implode('.', array_filter([$this->getNameOfPolicyFromProperty(), $type]));
    }

    protected function getGatePolicyType(): string
    {
        if (property_exists($this, 'policyType')) {
            return $this->policyType->toMethodName();
        }

        return PolicyType::VIEW->toMethodName();
    }

    protected function getNameOfObject(mixed $object): string
    {
        if (is_object($object)) {
            return $object::class;
        }

        return (string) $object;
    }

    protected function getNameOfPolicy(mixed $object): string
    {
        return $this->getNameOfPolicyFromProperty() ?? $this->getNameOfObject($object);
    }

    protected function getNameOfPolicyFromProperty(): ?string
    {
        return $this->nameOfPolicy;
    }
}
