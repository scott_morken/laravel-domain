<?php

declare(strict_types=1);

namespace Smorken\Domain\Authorization\Contracts;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Authenticatable;

interface SelfDefiningPolicy
{
    public function after(Authenticatable $user, string $ability): ?bool;

    public function before(Authenticatable $user, string $ability): ?bool;

    public static function defineAbilities(string $baseName, Gate $gate): void;

    public static function defineSelf(string $className, Gate $gate): void;
}
