<?php

declare(strict_types=1);

namespace Smorken\Domain\Authorization\Contracts;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Model\Contracts\Model;

interface ModelBasedPolicy
{
    public function create(Authenticatable $user, ?array $attributes = null): bool|Response;

    public function delete(Authenticatable $user, Model $model): bool|Response;

    public function destroy(Authenticatable $user, Model $model): bool|Response;

    public function forceDelete(Authenticatable $user, Model $model): bool|Response;

    public function index(Authenticatable $user): bool|Response;

    public function restore(Authenticatable $user, Model $model): bool|Response;

    public function update(Authenticatable $user, Model $model): bool|Response;

    public function upsert(Authenticatable $user, Model $model): bool|Response;

    public function view(Authenticatable $user, Model $model): bool|Response;

    public function viewAny(Authenticatable $user): bool|Response;
}
