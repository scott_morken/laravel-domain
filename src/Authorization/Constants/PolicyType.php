<?php

declare(strict_types=1);

namespace Smorken\Domain\Authorization\Constants;

enum PolicyType: string
{
    case CREATE = 'create';
    case DELETE = 'delete';
    case DESTROY = 'destroy';
    case FORCE_DELETE = 'forceDelete';
    case INDEX = 'index';
    case RESTORE = 'restore';
    case UPDATE = 'update';
    case UPSERT = 'upsert';
    case VIEW = 'view';
    case VIEW_ANY = 'viewAny';

    public function toMethodName(): string
    {
        return $this->value;
    }
}
