<?php

declare(strict_types=1);

namespace Smorken\Domain\Authorization\Policies;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Authenticatable;

abstract class BasePolicy implements \Smorken\Domain\Authorization\Contracts\SelfDefiningPolicy
{
    public function after(Authenticatable $user, string $ability): ?bool
    {
        return null;
    }

    public function before(Authenticatable $user, string $ability): ?bool
    {
        return null;
    }

    public static function defineAbilities(string $baseName, Gate $gate): void
    {
        $skip = ['before', 'after', 'defineAbilities', 'defineSelf'];
        $methods = [];
        $ref = new \ReflectionClass(static::class);
        foreach ($ref->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
            $name = $method->getName();
            if (in_array($name, $skip)) {
                continue;
            }
            $methods[$name] = $name;
        }
        $gate->resource($baseName, static::class, $methods);
    }

    public static function defineSelf(string $className, Gate $gate): void
    {
        $gate->policy($className, static::class);
    }

    /**
     * Create a new access response.
     */
    protected function allow(?string $message = null, mixed $code = null): Response
    {
        return Response::allow($message, $code);
    }

    /**
     * Throws an unauthorized exception.
     */
    protected function deny(?string $message = null, mixed $code = null): Response
    {
        return Response::deny($message, $code);
    }

    /**
     * Deny with a 404 HTTP status code.
     */
    protected function denyAsNotFound(?string $message = null, ?int $code = null): Response
    {
        return Response::denyWithStatus(404, $message, $code);
    }

    /**
     * Deny with an HTTP status code.
     */
    protected function denyWithStatus(int $status, ?string $message = null, ?int $code = null): Response
    {
        return Response::denyWithStatus($status, $message, $code);
    }
}
