<?php

declare(strict_types=1);

namespace Smorken\Domain\Authorization\Policies;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Model\Contracts\Model;

/**
 * @template TUser of Authenticatable
 * @template TModel of Model
 */
class ModelBasedPolicy extends BasePolicy implements \Smorken\Domain\Authorization\Contracts\ModelBasedPolicy
{
    /**
     * @param  TUser  $user
     */
    public function create(Authenticatable $user, ?array $attributes = null): bool|Response
    {
        return $this->isUserAllowedToCreate($user, $attributes);
    }

    /**
     * @param  TUser  $user
     * @param  TModel  $model
     */
    public function delete(Authenticatable $user, Model $model): bool|Response
    {
        return $this->destroy($user, $model);
    }

    /**
     * @param  TUser  $user
     * @param  TModel  $model
     */
    public function destroy(Authenticatable $user, Model $model): bool|Response
    {
        return $this->isUserAllowedToDestroy($user, $model);
    }

    /**
     * @param  TUser  $user
     * @param  TModel  $model
     */
    public function forceDelete(Authenticatable $user, Model $model): bool|Response
    {
        return $this->destroy($user, $model);
    }

    /**
     * @param  TUser  $user
     */
    public function index(Authenticatable $user): bool|Response
    {
        return $this->viewAny($user);
    }

    /**
     * @param  TUser  $user
     * @param  TModel  $model
     */
    public function restore(Authenticatable $user, Model $model): bool|Response
    {
        return $this->destroy($user, $model);
    }

    /**
     * @param  TUser  $user
     * @param  TModel  $model
     */
    public function update(Authenticatable $user, Model $model): bool|Response
    {
        return $this->isUserAllowedToUpdate($user, $model);
    }

    /**
     * @param  TUser  $user
     * @param  TModel  $model
     */
    public function upsert(Authenticatable $user, Model $model): bool|Response
    {
        if ($model->getKey() === null) {
            return $this->isUserAllowedToCreateOnUpsert($user, $model);
        }

        return $this->isUserAllowedToUpdate($user, $model);
    }

    /**
     * @param  TUser  $user
     * @param  TModel  $model
     */
    public function view(Authenticatable $user, Model $model): bool|Response
    {
        return $this->isUserAllowedToView($user, $model);
    }

    /**
     * @param  TUser  $user
     */
    public function viewAny(Authenticatable $user): bool|Response
    {
        return $this->isUserAllowedToViewAny($user);
    }

    /**
     * @param  TUser  $user
     */
    protected function isUserAllowedToCreate(Authenticatable $user, ?array $attributes = null): Response
    {
        return $this->deny('You do not have permission to create a new record.');
    }

    /**
     * @param  TUser  $user
     * @param  TModel  $model
     */
    protected function isUserAllowedToCreateOnUpsert(Authenticatable $user, Model $model): bool|Response
    {
        return $this->deny('You do not have permission to create a new record.');
    }

    /**
     * @param  TUser  $user
     * @param  TModel  $model
     */
    protected function isUserAllowedToDestroy(Authenticatable $user, Model $model): Response
    {
        return $this->deny('You do not have permission to delete this record.');
    }

    /**
     * @param  TUser  $user
     * @param  TModel  $model
     */
    protected function isUserAllowedToUpdate(Authenticatable $user, Model $model): Response
    {
        return $this->deny('You do not have permission to update this record.');
    }

    /**
     * @param  TUser  $user
     * @param  TModel  $model
     */
    protected function isUserAllowedToView(Authenticatable $user, Model $model): Response
    {
        return $this->deny('You do not have permission to view this record.');
    }

    /**
     * @param  TUser  $user
     */
    protected function isUserAllowedToViewAny(Authenticatable $user): Response
    {
        return $this->deny('You do not have permission to view these records.');
    }
}
