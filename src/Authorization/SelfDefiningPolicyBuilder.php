<?php

declare(strict_types=1);

namespace Smorken\Domain\Authorization;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Foundation\Application;

class SelfDefiningPolicyBuilder
{
    public function __construct(
        protected Gate $gate,
        protected Application $app
    ) {}

    /**
     * @param  array<class-string, class-string>  $policies
     */
    public function build(array $policies): void
    {
        foreach ($policies as $objectClass => $policyClass) {
            $policyClass::defineSelf($objectClass, $this->gate);
        }
    }
}
