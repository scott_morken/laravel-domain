<?php

namespace Smorken\Domain\Cache\Concerns;

use Carbon\Carbon;
use Smorken\Domain\Cache\Constants\CacheType;
use Smorken\Domain\Cache\Contracts\CacheHandler;

trait HasCacheHandler
{
    protected ?CacheHandler $cacheHandler = null;

    protected mixed $cacheKey = 'cacheable';

    public function getCacheHandler(): CacheHandler
    {
        if (! $this->cacheHandler) {
            $this->cacheHandler = new \Smorken\Domain\Cache\CacheHandler(static::class, $this->getCacheType());
        }

        return $this->cacheHandler;
    }

    public function resetCacheHandler(): void
    {
        if ($this->getCacheKey()) {
            $this->forgetCached();
        }
        $this->cacheHandler = null;
    }

    protected function cacheHandlerUseCache(bool $useCache): void
    {
        $this->getCacheHandler()->useCache($useCache);
    }

    protected function forgetCached(): void
    {
        $this->getCacheHandler()->forget($this->getCacheKey());
    }

    protected function getCacheKey(): string
    {
        return $this->cacheKey;
    }

    public function setCacheKey(mixed $key): void
    {
        $this->cacheKey = $key;
    }

    protected function getCacheType(): CacheType
    {
        if (property_exists($this, 'cacheType')) {
            return $this->cacheType;
        }

        return CacheType::INSTANCE;
    }

    protected function getDefaultCacheTTL(): Carbon
    {
        return $this->getCacheHandler()->getCacheAssist()->getCacheOptions()->defaultCacheTime;
    }
}
