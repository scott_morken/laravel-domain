<?php

declare(strict_types=1);

namespace Smorken\Domain\Cache\Contracts;

interface WithCacheHandler
{
    public function getCacheHandler(): CacheHandler;

    public function resetCacheHandler(): void;

    public function setCacheKey(mixed $key): void;
}
