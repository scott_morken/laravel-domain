<?php

namespace Smorken\Domain\Cache\Contracts;

use Smorken\CacheAssist\Contracts\CacheAssist;

interface CacheHandler
{
    public function forget(...$keys): int;

    public function getCacheAssist(): CacheAssist;

    public function remember(string $key, \DateInterval|\DateTimeInterface|int|null $ttl, \Closure $callback, bool $retryOnNull = true): mixed;

    public function useCache(bool $useCache): void;
}
