<?php

namespace Smorken\Domain\Cache\Constants;

enum CacheType
{
    case INSTANCE;
    case PERSIST;

    case NONE;

    case BOTH;
}
