<?php

namespace Smorken\Domain\Cache;

use Smorken\CacheAssist\HasCacheAssist;
use Smorken\Domain\Cache\Constants\CacheType;

class CacheHandler implements \Smorken\Domain\Cache\Contracts\CacheHandler
{
    use HasCacheAssist;

    protected static ?CacheHandler $instance = null;

    protected array $cached = [];

    protected bool $useCache = true;

    public function __construct(protected string $baseName, protected CacheType $cacheType = CacheType::BOTH)
    {
        $this->cacheAssist = $this->createCacheAssist(['baseName' => $baseName]);
    }

    public function forget(...$keys): int
    {
        $this->cached = [];
        if ($this->wantsPersistCache() && $keys) {
            return $this->getCacheAssist()->forget($keys);
        }

        return 0;
    }

    public function remember(string $key, \DateInterval|\DateTimeInterface|int|null $ttl, \Closure $callback, bool $retryOnNull = true): mixed
    {
        if ($this->useCache && array_key_exists($key, $this->cached)) {
            return $this->cached[$key];
        }
        $v = $this->persistOrReturnValue($key, $ttl, $callback, $retryOnNull);
        $this->toInstanceCache($key, $v);

        return $v;
    }

    public function useCache(bool $useCache): void
    {
        $this->useCache = $useCache;
    }

    protected function persistOrReturnValue(
        string $key,
        \DateInterval|\DateTimeInterface|int|null $ttl,
        \Closure $callback,
        bool $retryOnNull
    ): mixed {
        if ($this->useCache && $this->wantsPersistCache()) {
            $v = $this->getCacheAssist()->remember([$key], $ttl, $callback);
            if ($v !== null || $retryOnNull === false) {
                return $v;
            }
        }

        return $callback();
    }

    protected function toInstanceCache(string $key, mixed $value): void
    {
        if ($this->wantsInstanceCache()) {
            $this->cached[$key] = $value;
        }
    }

    protected function wantsInstanceCache(): bool
    {
        return $this->cacheType === CacheType::INSTANCE || $this->cacheType === CacheType::BOTH;
    }

    protected function wantsPersistCache(): bool
    {
        return $this->cacheType === CacheType::PERSIST || $this->cacheType === CacheType::BOTH;
    }
}
