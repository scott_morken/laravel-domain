<?php

namespace Smorken\Domain;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Smorken\Domain\Support\CacheCircuitBreaker;
use Smorken\Domain\Support\Contracts\CircuitBreaker;

class CircuitBreakerServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(CircuitBreaker::class, function (Application $app) {
            CacheCircuitBreaker::setCacheManager($app['cache']);

            return new CacheCircuitBreaker;
        });
    }
}
