<?php

namespace Smorken\Domain\Models\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Smorken\Model\Contracts\Model;

/**
 * @template TModel of \Smorken\Model\Contracts\Model|EloquentModel
 */
trait WithFindEloquentModel
{
    use WithBuilderHasQuery;

    /**
     * @template TBuilder of \Illuminate\Contracts\Database\Eloquent\Builder
     *
     * @param  TBuilder  $query
     * @return TModel|null
     */
    protected function findByIdUsingQuery(Builder $query, string|int|array $id, bool $throw): EloquentModel|Model|null
    {
        if ($this->builderHasQuery('upsertIdentifierIs', $query)) {
            return $this->queryByUpsertIdentifierIs($query, $id, $throw);
        }
        if ($this->builderHasQuery('idIs', $query)) {
            return $this->queryByIdIs($query, $id, $throw);
        }

        return $this->queryByFind($query, $id, $throw);
    }

    /**
     * @template TBuilder of \Illuminate\Contracts\Database\Eloquent\Builder
     *
     * @param  TBuilder  $query
     * @return TModel|null
     */
    protected function queryByFind(Builder $query, string|int|array $id, bool $throw): EloquentModel|Model|null
    {
        return $throw ? $query->findOrFail($id) : $query->find($id);
    }

    /**
     * @template TBuilder of \Illuminate\Contracts\Database\Eloquent\Builder
     *
     * @param  TBuilder  $query
     * @return TModel|null
     */
    protected function queryByIdIs(Builder $query, string|int|array $id, bool $throw): EloquentModel|Model|null
    {
        // @phpstan-ignore method.notFound
        $model = $query->idIs($id)->first();
        if (! $model && $throw) {
            throw new ModelNotFoundException;
        }

        return $model;
    }

    /**
     * @template TBuilder of \Illuminate\Contracts\Database\Eloquent\Builder
     *
     * @param  TBuilder  $query
     * @return TModel|null
     */
    protected function queryByUpsertIdentifierIs(Builder $query, string|int|array $id, bool $throw): EloquentModel|Model|null
    {
        // @phpstan-ignore method.notFound
        $model = $query->upsertIdentifierIs($id)->first();
        if (! $model && $throw) {
            throw new ModelNotFoundException;
        }

        return $model;
    }
}
