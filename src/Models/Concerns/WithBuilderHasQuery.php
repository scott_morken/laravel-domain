<?php

namespace Smorken\Domain\Models\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;

trait WithBuilderHasQuery
{
    /**
     * @template TBuilder of Builder
     *
     * @param  TBuilder  $query
     */
    protected function builderHasQuery(string $method, Builder $query): bool
    {
        if (method_exists($query, $method)) {
            return true;
        }
        $scoped = 'scope'.ucfirst($method);

        return method_exists($query->getModel(), $scoped);
    }
}
