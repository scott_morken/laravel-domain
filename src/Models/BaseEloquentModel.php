<?php

declare(strict_types=1);

namespace Smorken\Domain\Models;

use Smorken\Model\Eloquent;

class BaseEloquentModel extends Eloquent {}
