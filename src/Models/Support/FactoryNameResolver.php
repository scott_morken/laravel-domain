<?php

declare(strict_types=1);

namespace Smorken\Domain\Models\Support;

use Illuminate\Support\Str;

final class FactoryNameResolver
{
    protected function __construct(protected NamespacesVO $namespaces) {}

    public static function resolver(NamespacesVO $namespaces): \Closure
    {
        $resolver = new self($namespaces);

        return function (string $modelName) use ($resolver): string {
            return $resolver($modelName);
        };
    }

    public function __invoke(string $modelName): string
    {
        if ($this->wantsDomainNamespace($modelName)) {
            return $this->resolveWithDomainNamespace($modelName);
        }

        return $this->resolveWithBaseNamespace($modelName);
    }

    protected function wantsDomainNamespace(string $modelName): bool
    {
        if (Str::startsWith($modelName, $this->namespaces->domainNamespace)) {
            return true;
        }

        return false;
    }

    protected function resolveWithDomainNamespace(string $modelName): string
    {
        $parts = Str::of($modelName)->explode('\\');
        $domain = $parts[1] ?? null;
        $model = $parts->last();

        return str_replace('\\\\', '\\',
            implode('\\', array_filter([$this->namespaces->domainFactoryNamespace, $domain, $model.'Factory'])));
    }

    protected function resolveWithBaseNamespace(string $modelName): string
    {
        return str_replace('\\\\', '\\', $this->namespaces->baseFactoryNamespace.$modelName.'Factory');
    }
}
