<?php

declare(strict_types=1);

namespace Smorken\Domain\Models\Support;

use Illuminate\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

final class ModelNameResolver
{
    protected function __construct(protected NamespacesVO $namespaces) {}

    public static function resolver(NamespacesVO $namespaces): \Closure
    {
        $resolver = new self($namespaces);

        return function (Factory $factory) use ($resolver): string {
            return $resolver($factory::class);
        };
    }

    public function __invoke(string $factoryName): string
    {
        if ($this->wantsDomainNamespace($factoryName)) {
            return $this->resolveWithDomainNamespace($factoryName);
        }

        return $this->resolveWithBaseNamespace($factoryName);
    }

    protected function resolveWithBaseNamespace(string $factoryName): string
    {
        $baseName = Str::replaceLast(
            'Factory', '', Str::replaceFirst($this->namespaces->baseFactoryNamespace, '', $factoryName)
        );

        if (class_exists($baseName)) { // package factory
            return $baseName;
        }

        $factoryBasename = Str::replaceLast('Factory', '', $factoryName);

        $appNamespace = $this->appNamespace();

        return class_exists($appNamespace.'Models\\'.$baseName)
            ? $appNamespace.'Models\\'.$baseName
            : $appNamespace.$factoryBasename;
    }

    protected function resolveWithDomainNamespace(string $factoryName): string
    {
        $actualModelClassname = Str::replaceLast('Factory', '', class_basename($factoryName));
        $base = Str::replaceFirst($this->namespaces->domainFactoryNamespace, '', $factoryName);
        $baseParts = explode('\\', $base);
        $domainParts = [$baseParts[0] ?? null, 'Models'];
        $baseClassname = implode('\\',
            array_filter([...$domainParts, 'Eloquent', $actualModelClassname]));

        $eloquentBasedClassname = $this->namespaces->domainNamespace.$baseClassname;
        $simpleClassname = $this->namespaces->domainNamespace.implode('\\', $domainParts).'\\'.$actualModelClassname;

        return class_exists($eloquentBasedClassname)
            ? $eloquentBasedClassname
            : $simpleClassname;
    }

    protected function wantsDomainNamespace(string $factoryName): bool
    {
        if (Str::startsWith($factoryName, $this->namespaces->domainFactoryNamespace)) {
            return true;
        }

        return false;
    }

    protected function appNamespace(): string
    {
        try {
            return Container::getInstance()
                ->make(Application::class)
                ->getNamespace();
        } catch (\Throwable) {
            return 'App\\';
        }
    }
}
