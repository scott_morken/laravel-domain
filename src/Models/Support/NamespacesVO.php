<?php

declare(strict_types=1);

namespace Smorken\Domain\Models\Support;

class NamespacesVO
{
    public function __construct(
        public string $domainNamespace,
        public string $domainFactoryNamespace,
        public string $baseFactoryNamespace,
    ) {}
}
