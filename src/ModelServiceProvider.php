<?php

namespace Smorken\Domain;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\Domain\Models\Support\FactoryNameResolver;
use Smorken\Domain\Models\Support\ModelNameResolver;
use Smorken\Domain\Models\Support\NamespacesVO;

class ModelServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfigs();
        $domainNamespace = config('sm-domain.domain_namespace', 'Domain\\');
        $domainFactoryNamespace = config('sm-domain.domain_factory_namespace', 'Database\\Factories\\Eloquent\\');
        $baseFactoryNamespace = config('sm-domain.base_factory_namespace', 'Database\\Factories\\');
        // Factory::useNamespace($factoryNamespace);
        $ns = new NamespacesVO($domainNamespace, $domainFactoryNamespace, $baseFactoryNamespace);
        Factory::guessModelNamesUsing(ModelNameResolver::resolver($ns));
        Factory::guessFactoryNamesUsing(FactoryNameResolver::resolver($ns));
    }

    public function register(): void
    {
        $this->registerModels();
    }

    protected function bootConfigs(): void
    {
        $config = __DIR__.'/../config/models.php';
        $this->mergeConfigFrom($config, 'sm-models');
        $this->publishes([$config => config_path('sm-models.php')], 'config');
    }

    protected function registerModels(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-models', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }
}
