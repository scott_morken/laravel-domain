<?php

namespace Smorken\Domain\Repositories\Concerns;

trait HasPageName
{
    public function setPageName(string $pageName): self
    {
        $this->pageName = $pageName;

        return $this;
    }

    public function getPageName(): string
    {
        return $this->pageName;
    }
}
