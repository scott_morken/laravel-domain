<?php

namespace Smorken\Domain\Repositories\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Domain\Models\Concerns\WithBuilderHasQuery;
use Smorken\Model\Contracts\Model;

/**
 * @template TModel of EloquentModel|Model
 */
trait RepositoryFromEloquent
{
    use WithBuilderHasQuery;

    protected array $queryCallbacks = [];

    public function addQueryCallback(callable $queryCallback): self
    {
        $this->queryCallbacks[] = $queryCallback;

        return $this;
    }

    public function query(): Builder
    {
        return $this->modifyQuery($this->baseNewQuery());
    }

    protected function applyDefaultsToQuery(Builder $query): Builder
    {
        if ($this->builderHasQuery('defaultOrder', $query) && $this->shouldApplyDefaultOrder()) {
            // @phpstan-ignore method.notFound
            $query->defaultOrder();
        }
        if ($this->builderHasQuery('defaultWiths', $query) && $this->shouldApplyDefaultWiths()) {
            // @phpstan-ignore method.notFound
            $query->defaultWiths();
        }

        return $query;
    }

    protected function applyQueryCallbacksToQuery(Builder $query): Builder
    {
        foreach ($this->queryCallbacks as $queryCallback) {
            is_callable($queryCallback) && $queryCallback($query);
        }

        return $query;
    }

    protected function baseNewQuery(): Builder
    {
        $query = $this->eloquentModel()->newQuery();
        $query = $this->applyDefaultsToQuery($query);

        return $this->applyQueryCallbacksToQuery($query);
    }

    /**
     * @return TModel
     */
    protected function eloquentModel(): EloquentModel|Model
    {
        return $this->model();
    }

    protected function getColumns(): array
    {
        if (property_exists($this, 'columns')) {
            return $this->columns;
        }

        return ['*'];
    }

    protected function modifyQuery(Builder $query): Builder
    {
        return $query;
    }

    protected function shouldApplyDefaultOrder(): bool
    {
        if (property_exists($this, 'applyDefaultOrderToQuery')) {
            return $this->applyDefaultOrderToQuery;
        }

        return true;
    }

    protected function shouldApplyDefaultWiths(): bool
    {
        if (property_exists($this, 'applyDefaultWithsToQuery')) {
            return $this->applyDefaultWithsToQuery;
        }

        return true;
    }
}
