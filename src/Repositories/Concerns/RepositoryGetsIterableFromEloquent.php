<?php

namespace Smorken\Domain\Repositories\Concerns;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Collection;
use Smorken\Model\Contracts\Model;

/**
 * @template TModel of Model|EloquentModel
 */
trait RepositoryGetsIterableFromEloquent
{
    /** @use \Smorken\Domain\Repositories\Concerns\HasEloquentPaginatedOrCollectionResults<TModel> */
    use HasEloquentPaginatedOrCollectionResults;

    /**
     * @return iterable<array-key, TModel>|\Illuminate\Contracts\Pagination\Paginator<TModel>|\Illuminate\Support\Collection<array-key, TModel>
     */
    protected function getIterable(int $perPage): iterable|Paginator|Collection
    {
        $this->queryCallbacks = [];
        $query = $this->query();
        if ($perPage) {
            return $this->modifyPaginator($this->paginatedResults($query, $perPage));
        }

        return $this->modifyCollection($this->collectedResults($query));
    }
}
