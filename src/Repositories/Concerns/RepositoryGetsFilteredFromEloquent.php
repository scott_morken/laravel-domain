<?php

namespace Smorken\Domain\Repositories\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Collection;
use Smorken\Model\Contracts\Model;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

/**
 * @template TModel of Model|EloquentModel
 */
trait RepositoryGetsFilteredFromEloquent
{
    /** @use \Smorken\Domain\Repositories\Concerns\HasEloquentPaginatedOrCollectionResults<TModel> */
    use HasEloquentPaginatedOrCollectionResults;

    public function applyFiltersToQuery(Filter|QueryStringFilter $filter, Builder $query): Builder
    {
        if (is_a($filter, QueryStringFilter::class)) {
            return $this->applyQueryStringFilterToQuery($filter, $query);
        }

        return $this->applyFilterToQuery($filter, $query);
    }

    protected function applyFilterToQuery(Filter $filter, Builder $query): Builder
    {
        // @phpstan-ignore method.notFound
        return $query->filter($filter);
    }

    protected function applyQueryStringFilterToQuery(QueryStringFilter $filter, Builder $query): Builder
    {
        if ($this->builderHasQuery('queryStringFilter', $query)) {
            // @phpstan-ignore method.notFound
            return $query->queryStringFilter($filter);
        }

        return $this->applyFilterToQuery(new \Smorken\Support\Filter($filter->filters()->toArray()), $query);
    }

    /**
     * @return iterable<array-key, TModel>|\Illuminate\Contracts\Pagination\Paginator<TModel>|\Illuminate\Support\Collection<array-key,TModel>
     */
    protected function getFiltered(Filter|QueryStringFilter $filter, int $perPage): iterable|Paginator|Collection
    {
        $filter = $this->modifyFilter($filter);
        $this->queryCallbacks = [];
        $this->addQueryCallback(fn (Builder $query) => $this->applyFiltersToQuery($filter, $query));
        $query = $this->query();
        if ($perPage) {
            return $this->modifyPaginator($this->paginatedResults($query, $perPage));
        }

        return $this->modifyCollection($this->collectedResults($query));
    }

    protected function modifyFilter(Filter|QueryStringFilter $filter): Filter|QueryStringFilter
    {
        return $filter;
    }
}
