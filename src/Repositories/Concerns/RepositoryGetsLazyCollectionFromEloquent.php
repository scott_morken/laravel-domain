<?php

namespace Smorken\Domain\Repositories\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\LazyCollection;
use Smorken\Model\Contracts\Model;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

/**
 * @template TModel of Model|EloquentModel
 */
trait RepositoryGetsLazyCollectionFromEloquent
{
    /** @use \Smorken\Domain\Repositories\Concerns\RepositoryFromEloquent<TModel> */
    use RepositoryFromEloquent;

    protected int $chunkSize = 0;

    public function setChunkSize(int $chunkSize): self
    {
        $this->chunkSize = $chunkSize;

        return $this;
    }

    public function applyFiltersToQuery(Filter|QueryStringFilter $filter, Builder $query): Builder
    {
        if (is_a($filter, QueryStringFilter::class)) {
            return $this->applyQueryStringFilterToQuery($filter, $query);
        }

        return $this->applyFilterToQuery($filter, $query);
    }

    protected function applyFilterToQuery(Filter $filter, Builder $query): Builder
    {
        // @phpstan-ignore method.notFound
        return $query->filter($filter);
    }

    protected function applyQueryStringFilterToQuery(QueryStringFilter $filter, Builder $query): Builder
    {
        if ($this->builderHasQuery('queryStringFilter', $query)) {
            // @phpstan-ignore method.notFound
            return $query->queryStringFilter($filter);
        }

        return $this->applyFilterToQuery(new \Smorken\Support\Filter($filter->filters()->toArray()), $query);
    }

    /**
     * @return \Illuminate\Support\LazyCollection<array-key, TModel>
     */
    protected function getLazyCollection(
        Filter|QueryStringFilter|null $filter
    ): LazyCollection {
        $this->queryCallbacks = [];
        if ($filter) {
            $this->addQueryCallback(fn (Builder $query) => $this->applyFiltersToQuery($filter, $query));
        }
        $query = $this->query();
        if ($this->chunkSize) {
            /** @var TModel */
            return $query->lazy($this->chunkSize);
        }

        /** @var TModel */
        return $query->cursor();
    }
}
