<?php

namespace Smorken\Domain\Repositories\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Domain\Models\Concerns\WithFindEloquentModel;
use Smorken\Model\Contracts\Model;

/**
 * @template TModel of Model|EloquentModel
 */
trait RepositoryRetrievesFromEloquent
{
    /** @use \Smorken\Domain\Repositories\Concerns\RepositoryFromEloquent<TModel> */
    use RepositoryFromEloquent;

    /** @use WithFindEloquentModel<TModel> */
    use WithFindEloquentModel;

    /**
     * @return TModel|null
     */
    protected function findById(string|int|array $id, bool $throw): EloquentModel|Model|null
    {
        $this->addQueryCallback(fn (Builder $query) => $query->select($this->getColumns()));
        $query = $this->query();

        return $this->findByIdUsingQuery($query, $id, $throw);
    }

    /**
     * @return TModel|null
     */
    protected function retrieve(mixed $id, bool $throw): EloquentModel|Model|null
    {
        $this->queryCallbacks = [];

        return $this->findById($id, $throw);
    }
}
