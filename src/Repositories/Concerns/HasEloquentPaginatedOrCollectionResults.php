<?php

declare(strict_types=1);

namespace Smorken\Domain\Repositories\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * @template TModel of \Smorken\Model\Contracts\Model|\Illuminate\Database\Eloquent\Model
 */
trait HasEloquentPaginatedOrCollectionResults
{
    use HasPageName;

    /** @use \Smorken\Domain\Repositories\Concerns\RepositoryFromEloquent<TModel> */
    use RepositoryFromEloquent;

    /**
     * @param  \Illuminate\Support\Collection<array-key, TModel>  $models
     * @return \Illuminate\Support\Collection<array-key, TModel>
     */
    protected function modifyCollection(Collection $models): Collection
    {
        return $models;
    }

    /**
     * @param  \Illuminate\Contracts\Pagination\Paginator<TModel>  $paginator
     * @return \Illuminate\Contracts\Pagination\Paginator<TModel>
     */
    protected function modifyPaginator(Paginator $paginator): Paginator
    {
        return $paginator;
    }

    protected function collectedResults(Builder $query): Collection
    {
        /** @var Collection<array-key, TModel> */
        return $query->get($this->getColumns());
    }

    protected function paginatedResults(Builder $query, int $perPage): Paginator
    {
        /** @var Paginator<TModel> */
        return $query->paginate($perPage, $this->getColumns(), $this->getPageName());
    }
}
