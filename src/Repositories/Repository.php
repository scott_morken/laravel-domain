<?php

namespace Smorken\Domain\Repositories;

use Smorken\Domain\Authorization\Concerns\Authorizes;
use Smorken\Domain\Cache\Concerns\HasCacheHandler;
use Smorken\Domain\Cache\Constants\CacheType;

/**
 * @template TModel of object|array|null
 *
 * @property TModel $model
 */
abstract class Repository implements \Smorken\Domain\Repositories\Contracts\Repository
{
    use Authorizes, HasCacheHandler;

    protected CacheType $cacheType = CacheType::INSTANCE;

    /**
     * @return TModel
     */
    public function model(): mixed
    {
        // @phpstan-ignore property.notFound
        return $this->model;
    }

    public function reset(): self
    {
        $this->forgetCached();

        return $this;
    }

    public function useCache(bool $useCache): self
    {
        $this->cacheHandlerUseCache($useCache);

        return $this;
    }

    protected function shouldAuthorize(): bool
    {
        return true;
    }
}
