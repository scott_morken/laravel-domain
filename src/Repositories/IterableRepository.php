<?php

namespace Smorken\Domain\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Authorization\Constants\PolicyType;
use Smorken\Domain\Cache\Constants\CacheType;
use Smorken\Domain\Repositories\Concerns\HasPageName;

/**
 * @template TModel of object|array|null
 *
 * @extends Repository<TModel>
 */
abstract class IterableRepository extends Repository implements \Smorken\Domain\Repositories\Contracts\IterableRepository
{
    use HasPageName;

    protected mixed $cacheKey = 'iterable';

    protected string $pageName = 'page';

    /**
     * @return iterable<array-key, TModel>|\Illuminate\Contracts\Pagination\Paginator<TModel>|\Illuminate\Support\Collection<array-key, TModel>
     */
    abstract protected function getIterable(int $perPage): iterable|Paginator|Collection;

    /**
     * @return iterable<array-key, TModel>|\Illuminate\Contracts\Pagination\Paginator<TModel>|\Illuminate\Support\Collection<array-key, TModel>
     */
    public function __invoke(int $perPage = 20): iterable|Paginator|Collection
    {
        if ($perPage && $this->cacheTypeHasPersistence()) {
            $this->cacheType = CacheType::INSTANCE;
        }
        if ($this->shouldAuthorize()) {
            $this->authorize($this->model(), PolicyType::VIEW_ANY);
        }

        return $this->getCacheHandler()->remember(
            $this->getCacheKey(),
            $this->getDefaultCacheTTL(),
            fn () => $this->getIterable($perPage)
        );
    }

    protected function cacheTypeHasPersistence(): bool
    {
        return $this->cacheType === CacheType::BOTH || $this->cacheType === CacheType::PERSIST;
    }
}
