<?php

declare(strict_types=1);

namespace Smorken\Domain\Repositories;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Domain\Repositories\Concerns\RepositoryGetsIterableFromEloquent;
use Smorken\Domain\Repositories\Contracts\RepositoryHasEloquent;
use Smorken\Model\Contracts\Model;

/**
 * @template TModel of EloquentModel|\Smorken\Model\Contracts\Model
 *
 * @extends \Smorken\Domain\Repositories\IterableRepository<TModel>
 */
class EloquentIterableRepository extends IterableRepository implements RepositoryHasEloquent
{
    /** @use RepositoryGetsIterableFromEloquent<TModel> */
    use RepositoryGetsIterableFromEloquent;

    /**
     * @param  TModel  $model
     */
    public function __construct(protected EloquentModel|Model $model) {}
}
