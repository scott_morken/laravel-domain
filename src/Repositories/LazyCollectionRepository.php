<?php

namespace Smorken\Domain\Repositories;

use Illuminate\Support\LazyCollection;
use Smorken\Domain\Authorization\Constants\PolicyType;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

/**
 * @template TModel of object|array|null
 *
 * @extends Repository<TModel>
 */
abstract class LazyCollectionRepository extends Repository implements \Smorken\Domain\Repositories\Contracts\LazyCollectionRepository
{
    /**
     * @return \Illuminate\Support\LazyCollection<array-key, TModel>
     */
    abstract protected function getLazyCollection(
        Filter|QueryStringFilter|null $filter
    ): LazyCollection;

    /**
     * @return \Illuminate\Support\LazyCollection<array-key, TModel>
     */
    public function __invoke(Filter|QueryStringFilter|null $filter = null): LazyCollection
    {
        if ($this->shouldAuthorize()) {
            $this->authorize($this->model(), PolicyType::VIEW_ANY);
        }

        return $this->getLazyCollection($filter);
    }
}
