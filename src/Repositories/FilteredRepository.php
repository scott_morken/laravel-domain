<?php

namespace Smorken\Domain\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Authorization\Constants\PolicyType;
use Smorken\Domain\Cache\Constants\CacheType;
use Smorken\Domain\Repositories\Concerns\HasPageName;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

/**
 * @template TModel of object|array|null
 *
 * @extends Repository<TModel>
 */
abstract class FilteredRepository extends Repository implements \Smorken\Domain\Repositories\Contracts\FilteredRepository
{
    use HasPageName;

    protected mixed $cacheKey = 'filtered';

    protected CacheType $cacheType = CacheType::INSTANCE;

    protected string $pageName = 'page';

    /**
     * @return iterable<array-key, TModel>|\Illuminate\Contracts\Pagination\Paginator<TModel>|\Illuminate\Support\Collection<array-key,
     *     TModel>
     */
    abstract protected function getFiltered(
        Filter|QueryStringFilter $filter,
        int $perPage
    ): iterable|Paginator|Collection;

    /**
     * @return iterable<array-key, TModel>|\Illuminate\Contracts\Pagination\Paginator<TModel>|\Illuminate\Support\Collection<array-key,
     *     TModel>
     */
    public function __invoke(Filter|QueryStringFilter $filter, int $perPage = 20): iterable|Paginator|Collection
    {
        if ($this->shouldAuthorize()) {
            $this->authorize($this->model(), PolicyType::VIEW_ANY);
        }

        return $this->getCacheHandler()->remember(
            $this->getCacheKey(),
            $this->getDefaultCacheTTL(),
            fn () => $this->getFiltered($this->modifyFilter($filter), $perPage)
        );
    }

    protected function modifyFilter(Filter|QueryStringFilter $filter): Filter|QueryStringFilter
    {
        return $filter;
    }
}
