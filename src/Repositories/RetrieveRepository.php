<?php

namespace Smorken\Domain\Repositories;

use Smorken\Domain\Authorization\Constants\PolicyType;
use Smorken\Domain\Cache\Constants\CacheType;
use Smorken\Model\Contracts\Model;

/**
 * @template TModel of object|array|null
 *
 * @extends Repository<TModel>
 */
abstract class RetrieveRepository extends Repository implements \Smorken\Domain\Repositories\Contracts\RetrieveRepository
{
    protected CacheType $cacheType = CacheType::BOTH;

    protected mixed $id = null;

    /**
     * @return TModel
     */
    abstract protected function retrieve(mixed $id, bool $throw): mixed;

    /**
     * @return TModel
     */
    public function __invoke(mixed $id, bool $throw = true): mixed
    {
        if (is_a($id, Model::class)) {
            return $id;
        }
        $this->id = $id;

        return $this->getCacheHandler()->remember(
            $this->getCacheKey(),
            $this->getDefaultCacheTTL(),
            fn () => $this->retrieveAndAuthorize($id, $throw)
        );
    }

    /**
     * @return TModel
     */
    public function emptyModel(): mixed
    {
        $model = $this->model();
        if (is_a($model, Model::class)) {
            // @phpstan-ignore method.notFound
            return $model->newInstance();
        }
        if (is_object($model) && method_exists($model, 'toArray')) {
            return $model->toArray();
        }

        return $this->createEmptyOverride($model);
    }

    public function setCacheKey(mixed $key): void
    {
        $this->id = $key;
    }

    /**
     * @param  TModel  $model
     */
    protected function createEmptyOverride(mixed $model): mixed
    {
        return [];
    }

    protected function getCacheKey(): string
    {
        return $this->getIdAsString($this->id);
    }

    protected function getIdAsString(mixed $id): string
    {
        if ($id instanceof \BackedEnum) {
            $id = $id->value;
        }
        if (is_array($id)) {
            $id = implode(',', $id);
        }

        return (string) $id;
    }

    /**
     * @return TModel
     */
    protected function retrieveAndAuthorize(mixed $id, bool $throw = true): mixed
    {
        $model = $this->retrieve($id, $throw);
        if ($model && $this->shouldAuthorize()) {
            $this->authorize($model, PolicyType::VIEW);
        }

        return $model;
    }
}
