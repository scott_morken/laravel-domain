<?php

declare(strict_types=1);

namespace Smorken\Domain\Repositories\Contracts;

use Illuminate\Support\LazyCollection;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

interface LazyCollectionRepository extends Repository
{
    public function __invoke(Filter|QueryStringFilter|null $filter = null): LazyCollection;
}
