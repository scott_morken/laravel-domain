<?php

namespace Smorken\Domain\Repositories\Contracts;

use Smorken\Domain\Cache\Contracts\WithCacheHandler;

interface Repository extends WithCacheHandler
{
    public function model(): mixed;

    public function reset(): self;

    public function useCache(bool $useCache): self;
}
