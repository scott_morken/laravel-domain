<?php

namespace Smorken\Domain\Repositories\Contracts;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

interface FilteredRepository extends Repository
{
    public function __invoke(Filter|QueryStringFilter $filter, int $perPage = 20): iterable|Paginator|Collection;

    public function setPageName(string $pageName): self;
}
