<?php

namespace Smorken\Domain\Repositories\Contracts;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

interface IterableRepository extends Repository
{
    public function __invoke(int $perPage = 20): iterable|Paginator|Collection;

    public function setPageName(string $pageName): self;
}
