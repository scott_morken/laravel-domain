<?php

declare(strict_types=1);

namespace Smorken\Domain\Repositories\Contracts;

use Illuminate\Contracts\Database\Eloquent\Builder;

interface RepositoryHasEloquent
{
    public function addQueryCallback(callable $queryCallback): self;

    public function query(): Builder;
}
