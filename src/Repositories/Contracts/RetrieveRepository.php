<?php

namespace Smorken\Domain\Repositories\Contracts;

interface RetrieveRepository extends Repository
{
    public function __invoke(mixed $id, bool $throw = true): mixed;

    public function emptyModel(): mixed;
}
