<?php

declare(strict_types=1);

namespace Smorken\Domain\Repositories;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Domain\Repositories\Concerns\RepositoryGetsLazyCollectionFromEloquent;
use Smorken\Domain\Repositories\Contracts\RepositoryHasEloquent;
use Smorken\Model\Contracts\Model;

/**
 * @template TModel of EloquentModel|\Smorken\Model\Contracts\Model
 *
 * @extends \Smorken\Domain\Repositories\LazyCollectionRepository<TModel>
 */
class EloquentLazyCollectionRepository extends LazyCollectionRepository implements RepositoryHasEloquent
{
    /** @use \Smorken\Domain\Repositories\Concerns\RepositoryGetsLazyCollectionFromEloquent<TModel> */
    use RepositoryGetsLazyCollectionFromEloquent;

    /**
     * @param  TModel  $model
     */
    public function __construct(protected EloquentModel|Model $model) {}
}
