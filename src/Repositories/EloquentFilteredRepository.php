<?php

declare(strict_types=1);

namespace Smorken\Domain\Repositories;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Domain\Repositories\Concerns\RepositoryGetsFilteredFromEloquent;
use Smorken\Domain\Repositories\Contracts\RepositoryHasEloquent;
use Smorken\Model\Contracts\Model;

/**
 * @template TModel of EloquentModel|\Smorken\Model\Contracts\Model
 *
 * @extends \Smorken\Domain\Repositories\FilteredRepository<TModel>
 */
class EloquentFilteredRepository extends FilteredRepository implements RepositoryHasEloquent
{
    /** @use RepositoryGetsFilteredFromEloquent<TModel> */
    use RepositoryGetsFilteredFromEloquent;

    /**
     * @param  TModel  $model
     */
    public function __construct(protected EloquentModel|Model $model) {}
}
