<?php

namespace Smorken\Domain;

use Illuminate\Contracts\Auth\Access\Gate;
use Smorken\Domain\Actions\Action;
use Smorken\Domain\Authorization\SelfDefiningPolicyBuilder;
use Smorken\Domain\Repositories\Repository;

class PolicyServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfigs();
        Action::setGate($this->app[Gate::class]);
        Repository::setGate($this->app[Gate::class]);
    }

    public function register(): void
    {
        $this->booted(function () {
            $this->registerPolicies();
        });
    }

    protected function bootConfigs(): void
    {
        $config = __DIR__.'/../config/policies.php';
        $this->mergeConfigFrom($config, 'sm-policies');
        $this->publishes([$config => config_path('sm-policies.php')], 'config');
    }

    protected function registerPolicies(): void
    {
        $builder = new SelfDefiningPolicyBuilder($this->app[Gate::class], $this->app);
        $builder->build($this->app['config']->get('sm-policies', []));
    }
}
