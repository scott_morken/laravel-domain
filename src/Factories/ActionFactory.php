<?php

declare(strict_types=1);

namespace Smorken\Domain\Factories;

use Illuminate\Contracts\Foundation\Application;
use Smorken\Domain\Actions\Contracts\Action;
use Smorken\Domain\Actions\Contracts\Results\DeleteResult;
use Smorken\Domain\Actions\Contracts\Results\SaveResult;
use Smorken\Domain\Actions\Contracts\Upsertable;

/**
 * @template TModel of \Smorken\Model\Contracts\Model|\Illuminate\Database\Eloquent\Model
 */
class ActionFactory
{
    /**
     * @var array<class-string, Action>
     */
    protected static array $actions = [];

    protected static Application $application;

    /**
     * @var array<string, class-string>
     */
    protected array $handlers = [];

    /**
     * @param  array<class-string, Action>  $actions
     */
    public function __construct(
        array $actions = []
    ) {
        self::$actions = [
            ...self::$actions,
            ...$actions,
        ];
    }

    /**
     * @param  array<class-string, Action>  $actions
     */
    public static function addActions(array $actions): void
    {
        self::$actions = [
            ...self::$actions,
            ...$actions,
        ];
    }

    public static function setApplication(Application $application): void
    {
        self::$application = $application;
    }

    public function delete(string|int $id, bool $authorizeData = true): DeleteResult
    {
        return $this->destroy($id, $authorizeData);
    }

    public function destroy(string|int $id, bool $authorizeData = true): DeleteResult
    {
        return $this->handlerForDestroy($id, $authorizeData);
    }

    /**
     * @template T of Action
     *
     * @param  class-string<T>  $contract
     */
    public function for(string $contract, array $invokeArgs = [], array $constructorArgs = []): mixed
    {
        $r = $this->make($contract, $constructorArgs);
        if (is_callable($r)) {
            return $r(...$invokeArgs);
        }

        return $r;
    }

    /**
     * @template T of Action
     *
     * @param  class-string<T>  $contract
     */
    public function forDelete(string $contract, mixed $id, bool $authorizeData = true): DeleteResult
    {
        return $this->for($contract, ['id' => $id, 'authorizeData' => $authorizeData]);
    }

    /**
     * @template T of Action
     *
     * @param  class-string<T>  $contract
     */
    public function forUpsert(string $contract, Upsertable $upsertable): SaveResult
    {
        return $this->for($contract, ['upsertable' => $upsertable]);
    }

    /**
     * @template T of Action
     *
     * @param  class-string<T>  $contract
     * @return T
     */
    public function make(string $contract, array $args = []): Action
    {
        $cls = $this->getConcreteClassName($contract);
        if (! $cls) {
            return $this->makingClass($contract, $contract, $args);
        }
        if (is_a($cls, Action::class)) {
            return $cls;
        }

        return $this->makingClass($contract, $cls, $args);
    }

    public function upsert(Upsertable $upsertable): SaveResult
    {
        return $this->handlerForUpsert($upsertable);
    }

    /**
     * @template T of Action
     *
     * @param  class-string<T>  $contract
     * @return class-string|Action|null
     */
    protected function getConcreteClassName(string $contract): string|Action|null
    {
        return self::$actions[$contract] ?? null;
    }

    /**
     * @return class-string|null
     */
    protected function getHandler(string $contract): ?string
    {
        return $this->getHandlers()[$contract] ?? null;
    }

    /**
     * @return array<string, class-string>
     */
    protected function getHandlers(): array
    {
        return $this->handlers ?? [];
    }

    protected function handlerForDestroy(string|int $id, bool $authorizeData = true): ?DeleteResult
    {
        if ($handler = $this->getHandler('delete')) {
            return $this->forDelete($handler, $id, $authorizeData);
        }

        return null;
    }

    protected function handlerForUpsert(Upsertable $upsertable): ?SaveResult
    {
        if ($handler = $this->getHandler('upsert')) {
            return $this->forUpsert($handler, $upsertable);
        }

        return null;
    }

    /**
     * @template TContract of Action|\Smorken\Domain\Actions\Action
     * @template TAction of Action|\Smorken\Domain\Actions\Action
     *
     * @param  class-string<TContract>  $contract
     * @param  class-string<TAction>  $makeClass
     * @return TAction
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function makingClass(string $contract, string $makeClass, array $args): Action
    {
        $made = self::$application->make($makeClass, $args);
        self::$actions[$contract] = $made;

        return $made;
    }
}
