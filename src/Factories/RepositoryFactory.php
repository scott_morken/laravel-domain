<?php

namespace Smorken\Domain\Factories;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Repositories\Contracts\Repository;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

/**
 * @template TModel of \Smorken\Model\Contracts\Model|\Illuminate\Database\Eloquent\Model
 *
 * @method iterable|Collection all()
 * @method iterable|Collection|Paginator filtered(Filter|QueryStringFilter $filter, int $perPage = 20)
 * @method iterable|Collection|Paginator get(int $perPage = 20)
 * @method mixed find(mixed $id, bool $throw = true)
 * @method mixed emptyModel()
 */
class RepositoryFactory
{
    protected static Application $application;

    /**
     * @var array<class-string, Repository>
     */
    protected static array $repositories = [];

    /**
     * @var array<string, class-string|null>
     */
    protected array $handlers = [
        'find' => null,
        'filtered' => null,
        'all' => null,
        'get' => null,
    ];

    protected array $resets = [];

    /**
     * @param  array<string, Repository>  $repositories
     */
    public function __construct(
        array $repositories = []
    ) {
        self::$repositories = [
            ...self::$repositories,
            ...$repositories,
        ];
    }

    /**
     * @param  array<string, Repository>  $repositories
     */
    public static function addRepositories(array $repositories): void
    {
        self::$repositories = [
            ...self::$repositories,
            ...$repositories,
        ];
    }

    public static function setApplication(Application $application): void
    {
        self::$application = $application;
    }

    /**
     * @param  class-string  $contract
     */
    public function for(string $contract, array $invokeArgs = [], array $constructorArgs = []): mixed
    {
        $r = $this->make($contract, $constructorArgs);
        if (is_callable($r)) {
            return $r(...$invokeArgs);
        }

        return $r;
    }

    /**
     * @param  class-string  $contract
     * @return iterable<TModel>|\Illuminate\Support\Collection<array-key, TModel>|\Illuminate\Contracts\Pagination\Paginator<TModel>
     */
    public function forFiltered(
        string $contract,
        Filter|QueryStringFilter $filter,
        int $perPage = 20
    ): iterable|Collection|Paginator {
        return $this->for($contract, ['filter' => $filter, 'perPage' => $perPage]);
    }

    /**
     * @param  class-string  $contract
     * @return iterable<TModel>|\Illuminate\Support\Collection<array-key, TModel>|\Illuminate\Contracts\Pagination\Paginator<TModel>
     */
    public function forIterable(string $contract, int $perPage = 20): iterable|Collection|Paginator
    {
        return $this->for($contract, ['perPage' => $perPage]);
    }

    /**
     * @param  class-string  $contract
     * @return TModel|null
     */
    public function forRetrieve(string $contract, mixed $id, bool $throw = true): mixed
    {
        return $this->for($contract, ['id' => $id, 'throw' => $throw]);
    }

    /**
     * @template T of Repository
     *
     * @param  class-string<T>  $contract
     * @return T
     */
    public function make(string $contract, array $args = []): Repository
    {
        $cls = $this->getConcreteClassName($contract);
        if (! $cls) {
            return $this->makingClass($contract, $contract, $args);
        }
        if (is_a($cls, Repository::class)) {
            return $cls;
        }

        return $this->makingClass($contract, $cls, $args);
    }

    public function reset(mixed $id): void
    {
        if ($id !== null) {
            $this->resetForFind($id);
        }
        $this->resetOthers($this->getOthersForReset());
    }

    /**
     * @param  class-string  $contract
     * @return class-string|\Smorken\Domain\Repositories\Contracts\Repository|null
     */
    protected function getConcreteClassName(string $contract): string|Repository|null
    {
        return self::$repositories[$contract] ?? null;
    }

    /**
     * @return class-string|null
     */
    protected function getHandler(string $key): ?string
    {
        return $this->getHandlers()[$key] ?? null;
    }

    /**
     * @return array<string, class-string|null>
     */
    protected function getHandlers(): array
    {
        return $this->handlers ?? [];
    }

    protected function getOthersForReset(): array
    {
        return $this->resets;
    }

    /**
     * @return iterable<TModel>|\Illuminate\Support\Collection<array-key, TModel>
     */
    protected function handlerForAll(): iterable|Collection
    {
        if ($handler = $this->getHandler('all')) {
            return $this->forIterable($handler, 0);
        }

        return new Collection;
    }

    /**
     * @return TModel|null
     */
    protected function handlerForEmptyModel(): mixed
    {
        if (($handler = $this->getHandler('find'))) {
            $r = $this->make($handler);

            if (method_exists($r, 'emptyModel')) {
                return $r->emptyModel();
            }

            return $r->model();
        }

        return null;
    }

    /**
     * @return iterable<TModel>|\Illuminate\Support\Collection<array-key, TModel>|\Illuminate\Contracts\Pagination\Paginator<TModel>
     */
    protected function handlerForFiltered(
        Filter|QueryStringFilter $filter,
        int $perPage = 20
    ): iterable|Collection|Paginator {
        if ($handler = $this->getHandler('filtered')) {
            return $this->forFiltered($handler, $filter, $perPage);
        }

        return new Collection;
    }

    /**
     * @return TModel|null
     */
    protected function handlerForFind(mixed $id, bool $throw = true): mixed
    {
        if ($handler = $this->getHandler('find')) {
            return $this->forRetrieve($handler, $id, $throw);
        }

        return null;
    }

    /**
     * @return iterable<TModel>|\Illuminate\Support\Collection<array-key, TModel>|\Illuminate\Contracts\Pagination\Paginator<TModel>
     */
    protected function handlerForGet(int $perPage = 20): iterable|Collection|Paginator
    {
        if ($handler = $this->getHandler('get')) {
            return $this->forIterable($handler, $perPage);
        }

        return new Collection;
    }

    /**
     * @template TContract of Repository|\Smorken\Domain\Repositories\Repository
     * @template TRepo of Repository|\Smorken\Domain\Repositories\Repository
     *
     * @param  class-string<TContract>  $contract
     * @param  class-string<TRepo>  $makeClass
     * @return TRepo
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function makingClass(string $contract, string $makeClass, array $args): Repository
    {
        $made = self::$application->make($makeClass, $args);
        self::$repositories[$contract] = $made;

        return $made;
    }

    protected function resetForContractAndKey(string $contract, mixed $key = null): void
    {
        $r = $this->make($contract);
        if ($key !== null) {
            $r->setCacheKey($key);
        }
        $r->reset();
    }

    protected function resetForFind(mixed $id): void
    {
        if ($handler = $this->getHandler('find')) {
            $this->resetForContractAndKey($handler, $id);
        }
    }

    protected function resetOthers(array $others): void
    {
        foreach ($others as $other) {
            $this->resetForContractAndKey($other);
        }
    }
}
