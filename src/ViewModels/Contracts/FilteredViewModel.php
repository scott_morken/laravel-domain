<?php

namespace Smorken\Domain\ViewModels\Contracts;

use Smorken\Domain\ViewModels\Contracts\Concerns\WithSelectList;

interface FilteredViewModel extends IterableViewModel, WithSelectList {}
