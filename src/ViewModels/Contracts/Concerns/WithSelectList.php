<?php

declare(strict_types=1);

namespace Smorken\Domain\ViewModels\Contracts\Concerns;

interface WithSelectList
{
    public function defaultSelected(): string|int|null;

    public function toSelectList(): array;

    public function withDefaultSelected(string|int $id): self;

    public function withId(string|\Closure $attribute): self;

    public function withLabel(string|\Closure $attribute): self;
}
