<?php

namespace Smorken\Domain\ViewModels\Contracts;

interface RetrieveViewModel extends ViewModel
{
    public function model(): mixed;
}
