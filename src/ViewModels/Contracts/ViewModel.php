<?php

namespace Smorken\Domain\ViewModels\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Responsable;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

interface ViewModel extends Arrayable, Responsable
{
    public function filter(): Filter|QueryStringFilter|null;

    public function setFilter(Filter|QueryStringFilter $filter): self;
}
