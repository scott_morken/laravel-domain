<?php

namespace Smorken\Domain\ViewModels\Contracts;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\ViewModels\Contracts\Concerns\WithSelectList;

interface IterableViewModel extends ViewModel, WithSelectList
{
    public function isPaginated(): bool;

    public function models(): iterable|Collection|Paginator;
}
