<?php

namespace Smorken\Domain\ViewModels;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\ViewModels\Concerns\WithSelectList;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

/**
 * @template TModel of array|object|null
 *
 * @extends IterableViewModel<TModel>
 */
class FilteredViewModel extends IterableViewModel implements \Smorken\Domain\ViewModels\Contracts\FilteredViewModel
{
    /** @uses WithSelectList<TModel> */
    use WithSelectList;

    public function __construct(
        Filter|QueryStringFilter $filter,
        iterable|Collection|Paginator $models
    ) {
        parent::__construct($models);
        $this->setFilter($filter);
    }
}
