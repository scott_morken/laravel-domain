<?php

namespace Smorken\Domain\ViewModels;

/**
 * @template TModel of array|object|null
 */
class RetrieveViewModel extends ViewModel implements Contracts\RetrieveViewModel
{
    /**
     * @param  TModel  $model
     */
    public function __construct(
        protected mixed $model
    ) {}

    /**
     * @return TModel
     */
    public function model(): mixed
    {
        return $this->model;
    }
}
