<?php

declare(strict_types=1);

namespace Smorken\Domain\ViewModels\Concerns;

/**
 * @template TModel
 */
trait WithSelectList
{
    protected string|int|null $defaultSelected = null;

    protected string|\Closure $idAttribute = 'id';

    protected string|\Closure|null $labelAttribute = null;

    public function defaultSelected(): string|int|null
    {
        return $this->defaultSelected;
    }

    public function toSelectList(): array
    {
        $list = [];
        foreach ($this->models() as $model) {
            $list[$this->createId($model)] = $this->createLabel($model);
        }

        return $list;
    }

    public function withDefaultSelected(string|int $id): self
    {
        $this->defaultSelected = $id;

        return $this;
    }

    public function withId(string|\Closure $attribute): self
    {
        $this->idAttribute = $attribute;

        return $this;
    }

    public function withLabel(string|\Closure $attribute): self
    {
        $this->labelAttribute = $attribute;

        return $this;
    }

    /**
     * @param  TModel  $model
     */
    protected function createId(mixed $model): string|int
    {
        if (is_callable($this->idAttribute)) {
            return ($this->idAttribute)($model);
        }
        if (is_array($model)) {
            return $model[$this->idAttribute] ?? '';
        }

        return $model->{$this->idAttribute} ?? '';
    }

    /**
     * @param  TModel  $model
     */
    protected function createLabel(mixed $model): string
    {
        if (is_null($this->labelAttribute)) {
            if (is_array($model)) {
                return json_encode($model);
            }

            return (string) $model;
        }
        if (is_callable($this->labelAttribute)) {
            return ($this->labelAttribute)($model);
        }
        if (is_array($model)) {
            return $model[$this->labelAttribute] ?? '';
        }

        return $model->{$this->labelAttribute} ?? '';
    }
}
