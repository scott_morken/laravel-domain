<?php

namespace Smorken\Domain\ViewModels;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\ViewModels\Concerns\WithSelectList;

/**
 * @template TModel of array|object|null
 */
class IterableViewModel extends ViewModel implements \Smorken\Domain\ViewModels\Contracts\IterableViewModel
{
    /** @uses WithSelectList<TModel> */
    use WithSelectList;

    /**
     * @param  iterable<array-key, TModel>|\Illuminate\Support\Collection<array-key, TModel>|\Illuminate\Contracts\Pagination\Paginator<TModel>  $models
     */
    public function __construct(
        protected iterable|Collection|Paginator $models
    ) {}

    public function isPaginated(): bool
    {
        return is_a($this->models(), Paginator::class);
    }

    /**
     * @return iterable<array-key, TModel>|\Illuminate\Support\Collection<array-key, TModel>|\Illuminate\Contracts\Pagination\Paginator<TModel>
     */
    public function models(): iterable|Collection|Paginator
    {
        return $this->models;
    }
}
