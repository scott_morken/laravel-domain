<?php

namespace Smorken\Domain\ViewModels;

use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

abstract class ViewModel extends \Spatie\ViewModels\ViewModel implements \Smorken\Domain\ViewModels\Contracts\ViewModel
{
    protected Filter|QueryStringFilter|null $filter = null;

    public function filter(): Filter|QueryStringFilter|null
    {
        return $this->filter;
    }

    public function setFilter(Filter|QueryStringFilter $filter): self
    {
        $this->filter = $filter;

        return $this;
    }
}
