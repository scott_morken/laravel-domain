<?php

namespace Smorken\Domain\Support\Helpers;

use Illuminate\Support\Collection;

class GetPublicMethods
{
    protected Collection $cache;

    protected bool $isCached = false;

    protected array $skipMethods = ['__construct', '__toString'];

    public function __construct(
        protected object $from,
        array $skipMethods = []
    ) {
        $this->cache = new Collection;
        $this->skipMethods = array_unique([...$skipMethods, ...$this->skipMethods]);
    }

    public function get(): Collection
    {
        if ($this->isCached) {
            return $this->cache;
        }
        $collectedMethods = new Collection(
            (new \ReflectionClass($this->from))->getMethods(\ReflectionMethod::IS_PUBLIC)
        );
        $methods = $collectedMethods
            ->reject(fn (\ReflectionMethod $method) => in_array($method->getName(), $this->skipMethods))
            ->filter(fn (\ReflectionMethod $method) => empty($method->getParameters()))
            ->map(fn (\ReflectionMethod $method) => $method->getName())
            ->values();

        return $this->toCache($methods);
    }

    protected function toCache(Collection $methods): Collection
    {
        $this->isCached = true;
        $this->cache = $methods;

        return $this->cache;
    }
}
