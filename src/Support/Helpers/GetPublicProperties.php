<?php

namespace Smorken\Domain\Support\Helpers;

use Illuminate\Support\Collection;

class GetPublicProperties
{
    protected Collection $cache;

    protected bool $isCached = false;

    public function __construct(
        protected object $from
    ) {
        $this->cache = new Collection;
    }

    public function get(): Collection
    {
        if ($this->isCached) {
            return $this->cache;
        }
        $collectedProperties = new Collection(
            (new \ReflectionClass($this->from))->getProperties(\ReflectionProperty::IS_PUBLIC)
        );
        $properties = $collectedProperties
            ->map(fn (\ReflectionProperty $property) => $property->getName())
            ->values();

        return $this->toCache($properties);
    }

    protected function toCache(Collection $properties): Collection
    {
        $this->isCached = true;
        $this->cache = $properties;

        return $this->cache;
    }
}
