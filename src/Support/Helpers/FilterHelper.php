<?php

namespace Smorken\Domain\Support\Helpers;

use Illuminate\Http\Request;
use Smorken\Sanitizer\Contracts\Sanitize;
use Smorken\Support\Contracts\Filter;

class FilterHelper
{
    protected static ?Sanitize $sanitize = null;

    protected array $defaults = [];

    protected array $hidden = [];

    protected array $requestKeys = [];

    public static function setSanitizer(Sanitize $sanitize): void
    {
        self::$sanitize = $sanitize;
    }

    public function make(Request $request, ?FilterParameters $parameters = null): Filter
    {
        if (self::$sanitize) {
            $request = $this->sanitizeRequest($request);
        }
        $filter = $this->createBaseFilter($this->defaults);
        $this->applyRequestToFilter($filter, $request);
        $filter->hide($this->hidden);
        $this->modifyFilterBeforeReturn($filter, $request, $parameters);

        return $filter;
    }

    protected function applyRequestToFilter(Filter $filter, Request $request): void
    {
        $attributes = ! $this->empty($this->requestKeys) ? $request->only($this->requestKeys) : $request->query();
        $filter->setAttributes($attributes);
    }

    protected function createBaseFilter(array $defaults): Filter
    {
        return new \Smorken\Support\Filter($defaults);
    }

    protected function empty(mixed $value): bool
    {
        return empty($value) && ! is_numeric($value);
    }

    protected function modifyFilterBeforeReturn(Filter $filter, Request $request, ?FilterParameters $parameters): void
    {
        // override if needed
    }

    protected function sanitizeRequest(Request $request): Request
    {
        return $request;
    }
}
