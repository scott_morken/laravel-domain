<?php

namespace Smorken\Domain\Support\Filters;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

class DateFilter implements Arrayable
{
    public function __construct(
        public readonly Carbon $startDate,
        public readonly Carbon $endDate,
    ) {}

    public static function since(string|Carbon $startDate): self
    {
        if (is_string($startDate)) {
            $startDate = Carbon::parse($startDate);
        }

        return new self(
            startDate: $startDate,
            endDate: Carbon::now()
        );
    }

    public static function thisMonth(): self
    {
        return self::since(
            Carbon::now()->startOfMonth()->startOfDay(),
        );
    }

    public static function thisWeek(): self
    {
        return self::since(
            Carbon::now()->startOfWeek()->startOfDay(),
        );
    }

    public static function thisYear(): self
    {
        return self::since(
            Carbon::now()->startOfYear()->startOfDay(),
        );
    }

    public static function today(): self
    {
        return self::since(
            Carbon::now()->startOfDay(),
        );
    }

    public function toArray(): array
    {
        return [
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
        ];
    }
}
