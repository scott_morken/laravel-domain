<?php

namespace Smorken\Domain\Support\Contracts;

/**
 * @property \Illuminate\Http\Request $request
 * @property \Smorken\Support\Contracts\Filter $filter
 */
interface FilterableRequest {}
