<?php

namespace Smorken\Domain\Support\Contracts;

/**
 * https://medium.com/@dotcom.software/circuit-breaker-pattern-in-php-10ee1b35e14d
 */
interface CircuitBreaker
{
    public function __invoke(
        string $circuitId,
        \Closure $onCircuitOpen,
        \Closure $onCircuitClosed,
        \Closure $circuitEvaluator,
        int $closeDelay = 60
    ): void;
}
