<?php

namespace Smorken\Domain\Support;

use Illuminate\Cache\CacheManager;
use Smorken\Domain\Support\Contracts\CircuitBreaker;

class CacheCircuitBreaker implements CircuitBreaker
{
    protected static CacheManager $cacheManager;

    public static function setCacheManager(CacheManager $cacheManager): void
    {
        self::$cacheManager = $cacheManager;
    }

    public function __invoke(
        string $circuitId,
        \Closure $onCircuitOpen,
        \Closure $onCircuitClosed,
        \Closure $circuitEvaluator,
        int $closeDelay = 60
    ): void {
        $cacheKey = $this->getCacheKey($circuitId);
        // if it is cached, the circuit is closed
        if ($this->getCacheManager()->has($cacheKey)) {
            $onCircuitClosed();

            return;
        }
        try {
            // execute the main code
            $onCircuitOpen();
        } catch (\Throwable $throwable) {
            if ($circuitEvaluator($throwable) === false) {
                // rethrow the exception since eval is false
                throw $throwable;
            }
            $this->getCacheManager()->set($cacheKey, true, $closeDelay);
            $onCircuitClosed();
        }
    }

    protected function getCacheKey(string $circuitId): string
    {
        return 'circuit_breaker.'.$circuitId;
    }

    protected function getCacheManager(): CacheManager
    {
        return self::$cacheManager;
    }
}
