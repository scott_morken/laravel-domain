<?php

namespace Smorken\Domain\Support\Concerns;

trait HasGetDataClass
{
    abstract public function getDefaultDataClass(): ?string;

    public function getDataClass(): ?string
    {
        return match (true) {
            /** @psalm-suppress UndefinedThisPropertyFetch */
            property_exists($this, 'dataClass') => $this->dataClass,
            method_exists($this, 'dataClass') => $this->dataClass(),
            default => $this->getDefaultDataClass(),
        };
    }
}
