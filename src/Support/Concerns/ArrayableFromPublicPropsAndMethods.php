<?php

namespace Smorken\Domain\Support\Concerns;

use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Smorken\Domain\Support\Helpers\GetPublicMethods;
use Smorken\Domain\Support\Helpers\GetPublicProperties;

/**
 * @property array<string> $skipMethods Override to set higher (abstract) level skip methods
 * @property array<string> $classLevelSkipMethods Override in concrete class to set skip methods
 */
trait ArrayableFromPublicPropsAndMethods
{
    protected array $baseSkipMethods = ['toArray'];

    protected array $cached = ['propertyValues' => [], 'methodValues' => []];

    protected array $loaded = ['propertyValues' => false, 'methodValues' => false];

    protected ?GetPublicMethods $methodHelper = null;

    protected ?GetPublicProperties $propertyHelper = null;

    public function toArray(): array
    {
        return [
            ...$this->getPropertyValues(),
            ...$this->getMethodValues(),
        ];
    }

    protected function additionalSkipMethods(): array
    {
        return [
            ...(property_exists($this, 'skipMethods') ? $this->skipMethods : []),
            ...(property_exists($this, 'classLevelSkipMethods') ? $this->classLevelSkipMethods : []),
        ];
    }

    protected function getKeyFromName(string $name): string
    {
        if ($this->shouldSnakeCaseKeys()) {
            return Str::snake($name);
        }

        return $name;
    }

    protected function getMethodHelper(): GetPublicMethods
    {
        if ($this->methodHelper === null) {
            $this->methodHelper = new GetPublicMethods($this, $this->getSkipMethods());
        }

        return $this->methodHelper;
    }

    protected function getMethodValues(): array
    {
        if ($this->loaded['methodValues']) {
            return $this->cached['methodValues'];
        }
        $collected = $this->getPublicMethods()->mapWithKeys(fn (
            string $method
        ) => [$this->getKeyFromName($method) => $this->getValueFromMethod($method)]);
        if (! $this->useNullValues()) {
            $collected = $collected->filter(fn (mixed $v) => ! $this->isEmpty($v));
        }

        return $this->toCache('methodValues', $collected->toArray());
    }

    protected function getPropertyHelper(): GetPublicProperties
    {
        if ($this->propertyHelper === null) {
            $this->propertyHelper = new GetPublicProperties($this);
        }

        return $this->propertyHelper;
    }

    protected function getPropertyValues(): array
    {
        if ($this->loaded['propertyValues']) {
            return $this->cached['propertyValues'];
        }
        $collected = $this->getPublicProperties()->mapWithKeys(fn (
            string $property
        ) => [$this->getKeyFromName($property) => $this->$property]);
        if (! $this->useNullValues()) {
            $collected = $collected->filter(fn (mixed $v) => ! $this->isEmpty($v));
        }

        return $this->toCache('propertyValues', $collected->toArray());
    }

    protected function getPublicMethods(): Collection
    {
        return $this->getMethodHelper()->get();
    }

    protected function getPublicProperties(): Collection
    {
        return $this->getPropertyHelper()->get();
    }

    protected function getSkipMethods(): array
    {
        return [...$this->baseSkipMethods, ...$this->additionalSkipMethods()];
    }

    protected function getValueFromMethod(string $methodName): mixed
    {
        $value = $this->$methodName();
        if ($value instanceof Htmlable) {
            return (string) $value;
        }

        return $value;
    }

    protected function isEmpty(mixed $value): bool
    {
        return empty($value) && ! is_numeric($value) && ! is_bool($value);
    }

    protected function shouldSnakeCaseKeys(): bool
    {
        if (property_exists($this, 'snakeCaseKeys')) {
            return $this->snakeCaseKeys;
        }

        return false;
    }

    protected function toCache(string $cacheKey, array $items): array
    {
        $this->loaded[$cacheKey] = true;
        $this->cached[$cacheKey] = $items;

        return $items;
    }

    protected function useNullValues(): bool
    {
        if (property_exists($this, 'useNullValues')) {
            return $this->useNullValues;
        }

        return true;
    }
}
