<?php

namespace Smorken\Domain\Support\Concerns;

use Smorken\Data\Constants\GatePolicyType;
use Smorken\Data\Contracts\Data;

trait AuthorizesDataObject
{
    protected bool $authorizationChecked = false;

    protected function authorizeData(Data $data): void
    {
        if (! $this->authorizationChecked) {
            $data->authorizeSelf($this->getGatePolicyType());
            $this->authorizationChecked = true;
        }
    }

    protected function authorizeDataCallback(\Closure $dataCallback): void
    {
        if (! $this->authorizationChecked) {
            $this->authorizeData($dataCallback());
        }
    }

    protected function getGatePolicyType(): GatePolicyType
    {
        if (property_exists($this, 'gatePolicyType')) {
            return $this->gatePolicyType;
        }

        return GatePolicyType::VIEW;
    }
}
