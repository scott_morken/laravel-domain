<?php

namespace Smorken\Domain\Support;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterableRequest implements \Smorken\Domain\Support\Contracts\FilterableRequest
{
    public Filter $filter;

    public Request $request;

    public function __construct(Request $request)
    {
        $this->request = $this->modifyRequest($request);
        $this->filter = $this->modifyFilter($this->request, new \Smorken\Support\Filter);
    }

    /**
     * Override this method to apply request to filter
     */
    protected function applyRequestToFilter(Request $request, Filter $filter): void
    {
        // override to create filter
    }

    protected function modifyFilter(Request $request, Filter $filter): Filter
    {
        $this->applyRequestToFilter($request, $filter);

        return $filter;
    }

    protected function modifyRequest(Request $request): Request
    {
        return $this->sanitizeRequest($request);
    }

    protected function sanitizeRequest(Request $request): Request
    {
        return $request;
    }
}
