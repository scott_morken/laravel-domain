<?php

namespace Smorken\Domain;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Domain\Actions\EloquentUpsertAction;
use Smorken\Domain\Factories\ActionFactory;

class ActionServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
        EloquentUpsertAction::setValidationFactory($this->app[Factory::class]);
        ActionFactory::addActions($this->app['config']->get('sm-actions', []));
    }

    public function register(): void
    {
        ActionFactory::setApplication($this->app);
        $this->registerActions();
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/actions.php';
        $this->mergeConfigFrom($config, 'sm-actions');
        $this->publishes([$config => config_path('sm-actions.php')], 'config');
    }

    protected function registerActions(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-actions', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }
}
