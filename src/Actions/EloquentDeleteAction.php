<?php

namespace Smorken\Domain\Actions;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Domain\Actions\Contracts\Results\DeleteResult;
use Smorken\Domain\Authorization\Constants\PolicyType;
use Smorken\Domain\Models\Concerns\WithFindEloquentModel;
use Smorken\Model\Contracts\Model;

/**
 * @template TModel of EloquentModel|Model
 *
 * @extends \Smorken\Domain\Actions\ActionWithEloquent<TModel>
 */
class EloquentDeleteAction extends ActionWithEloquent implements Contracts\DeleteAction
{
    /** @use WithFindEloquentModel<TModel> */
    use WithFindEloquentModel;

    protected bool $shouldDelete = true;

    public function __invoke(int|array|string $id, bool $authorizeData = true): DeleteResult
    {
        $model = $this->findModelById($id);

        return $this->delete($model, $authorizeData);
    }

    /**
     * @param  TModel  $model
     */
    public function delete(mixed $model, bool $authorizeData = true): DeleteResult
    {
        $id = $model->getKey();
        if ($authorizeData) {
            $this->authorize($model, PolicyType::DELETE);
        }
        $deleted = false;
        if ($this->shouldDelete($model)) {
            $this->preDeleteModelActions($model);
            $deleted = $model->delete();
        }

        return new Results\DeleteResult($id, $deleted, $this->getMessages());
    }

    /**
     * @return TModel
     */
    protected function findModelById(int|array|string $id): EloquentModel|Model
    {
        $query = $this->getModelInstance()->newQuery();

        return $this->findByIdUsingQuery($query, $id, true);
    }

    /**
     * @param  TModel  $model
     */
    protected function preDeleteModelActions(EloquentModel|Model $model): void
    {
        // override if needed
    }

    /**
     * @param  TModel  $model
     */
    protected function shouldDelete(EloquentModel|Model $model): bool
    {
        return $this->shouldDelete;
    }
}
