<?php

declare(strict_types=1);

namespace Smorken\Domain\Actions\Support;

use Illuminate\Support\Str;
use Smorken\Domain\Actions\Contracts\Support\KeyConverter;

class SnakeCaseKeyConvert implements KeyConverter
{
    public function convert(string $key): string
    {
        return Str::snake($key);
    }
}
