<?php

namespace Smorken\Domain\Actions;

use Illuminate\Contracts\Support\MessageBag;
use Smorken\Domain\Authorization\Concerns\Authorizes;

abstract class Action implements \Smorken\Domain\Actions\Contracts\Action
{
    use Authorizes;

    protected MessageBag $messages;

    public function __construct()
    {
        $this->messages = new \Illuminate\Support\MessageBag;
    }

    public function getMessages(): MessageBag
    {
        return $this->messages;
    }
}
