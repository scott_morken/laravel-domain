<?php

declare(strict_types=1);

namespace Smorken\Domain\Actions;

use Illuminate\Http\Request;
use Smorken\Domain\Actions\Contracts\Support\KeyConverter;
use Smorken\Model\Contracts\Model;

class Upsertable implements \Smorken\Domain\Actions\Contracts\Upsertable
{
    protected array $attributes = [];

    protected ?KeyConverter $keyConverter = null;

    protected ?string $keyConverterClass = null;

    protected array $only = [];

    public function __construct(
        array $data,
        protected array|int|string|null $identifier
    ) {
        $this->setAttributes($data);
    }

    public static function fromArray(array $data, array|int|string|null $identifier): static
    {
        if (is_array($identifier)) {
            foreach ($data as $k => $v) {
                if (array_key_exists($k, $identifier)) {
                    unset($data[$k]);
                }
            }
            if (count($identifier) === 1 && array_key_exists('id', $identifier)) {
                $identifier = $identifier['id'];
            }
        }

        // @phpstan-ignore new.static
        return new static($data, $identifier);
    }

    public static function fromModel(Model $model): static
    {
        return self::fromArray($model->toArray(), [$model->getKeyName() => $model->getKey()]);
    }

    public static function fromRequest(Request $request, array|int|string|null $identifier): static
    {
        return self::fromArray($request->all(), $identifier);
    }

    public function forCreate(array $skip = [], bool $includeIdentifier = false): array
    {
        return $this->skip($skip, $this->toArray($includeIdentifier));
    }

    public function forUpdate(array $skip = []): array
    {
        return $this->skip($skip, $this->toArray());
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes): self
    {
        foreach ($attributes as $k => $v) {
            $this->setAttribute($k, $v);
        }

        return $this;
    }

    public function getIdentifier(): array|int|string|null
    {
        if (is_array($this->identifier)) {
            return $this->convertKeys($this->identifier);
        }

        return $this->identifier;
    }

    public function setAttribute(string $key, mixed $value): self
    {
        if ($this->hasOnly($key) !== false) {
            $this->attributes[$key] = $value;
        }

        return $this;
    }

    public function setKeyConverterClass(string $keyConverterClass): self
    {
        $this->keyConverterClass = $keyConverterClass;

        return $this;
    }

    public function toArray(bool $includeIdentifier = false): array
    {
        $arrayed = $this->convertKeys($this->attributes);
        if (is_array($this->identifier)) {
            foreach ($this->convertKeys($this->identifier) as $k => $v) {
                if (! $includeIdentifier) {
                    unset($arrayed[$k]);
                } else {
                    $arrayed[$k] = $v;
                }
            }
        }
        foreach ($arrayed as $k => $v) {
            if ($this->hasOnly($k) === false) {
                unset($arrayed[$k]);
            }
        }

        return $arrayed;
    }

    protected function convertKey(string $key): string
    {
        $converter = $this->getKeyConverter();
        if ($converter) {
            return $converter->convert($key);
        }

        return $key;
    }

    protected function convertKeys(array $arr): array
    {
        $converted = [];
        foreach ($arr as $k => $v) {
            $k = $this->convertKey($k);
            $converted[$k] = $v;
        }

        return $converted;
    }

    protected function getKeyConverter(): ?KeyConverter
    {
        if (! $this->keyConverter && $this->keyConverterClass) {
            $this->keyConverter = new ($this->keyConverterClass)();
        }

        return $this->keyConverter;
    }

    public function setKeyConverter(KeyConverter $keyConverter): self
    {
        $this->keyConverter = $keyConverter;

        return $this;
    }

    protected function hasOnly(string $key): ?bool
    {
        if (empty($this->only)) {
            return null;
        }

        return in_array($key, $this->only);
    }

    protected function skip(array $skip, array $attributes): array
    {
        foreach ($attributes as $k => $v) {
            if (in_array($k, $skip)) {
                unset($attributes[$k]);
            }
        }

        return $attributes;
    }
}
