<?php

namespace Smorken\Domain\Actions;

use Illuminate\Database\Eloquent\Model;
use Smorken\Domain\Actions\Concerns\HasValidationFactory;
use Smorken\Domain\Actions\Contracts\Upsertable as UpsertableContract;
use Smorken\Domain\Actions\Contracts\UpsertAction;
use Smorken\Domain\Actions\Results\SaveResult;
use Smorken\Domain\Authorization\Constants\PolicyType;
use Smorken\Domain\Models\Concerns\WithFindEloquentModel;
use Smorken\Model\Contracts\Model as ModelContract;

/**
 * @template TModel of Model|ModelContract
 * @template TUpsertable of UpsertableContract
 *
 * @extends \Smorken\Domain\Actions\ActionWithEloquent<TModel>
 */
abstract class EloquentUpsertAction extends ActionWithEloquent implements UpsertAction
{
    /** @use \Smorken\Domain\Models\Concerns\WithFindEloquentModel<TModel> */
    use HasValidationFactory, WithFindEloquentModel;

    protected bool $authorized = false;

    protected bool $creating = false;

    protected bool $includeIdentifierOnCreate = false;

    protected bool $saved = false;

    protected bool $shouldUpsert = true;

    /**
     * @param  TUpsertable  $upsertable
     */
    public function __invoke(UpsertableContract $upsertable): Contracts\Results\SaveResult
    {
        $upsertable = $this->preExecuteFromUpsertable($upsertable);
        $model = null;
        if ($this->shouldUpsert()) {
            $model = $this->upsertModelFromUpsertable($upsertable);
            $model = $this->postExecuteFromUpsertable($model, $upsertable);
        }

        return $this->resultFromModel($model);
    }

    protected function empty(mixed $value): bool
    {
        return empty($value) && ! is_numeric($value) && ! is_bool($value);
    }

    /**
     * @return TModel|null
     */
    protected function findModelById(mixed $id): Model|ModelContract|null
    {
        $query = $this->getModelInstance()->newQuery();

        return $this->findByIdUsingQuery($query, $id, false);
    }

    /**
     * @param  TUpsertable  $upsertable
     * @return TModel
     */
    protected function findOrCreateFromUpsertable(UpsertableContract $upsertable): Model|ModelContract
    {
        $identifiers = $this->getIdentifiers($upsertable);
        $m = ! $this->empty($identifiers) ? $this->findModelById($identifiers) : null;
        if ($m) {
            return $this->handleUpdateModel($m, $upsertable);
        } else {
            return $this->handleCreateModel($upsertable);
        }
    }

    /**
     * @param  TUpsertable  $upsertable
     */
    protected function getCreateAttributes(UpsertableContract $upsertable): array
    {
        return $upsertable->forCreate([], $this->includeIdentifierOnCreate);
    }

    /**
     * @param  TUpsertable  $upsertable
     */
    protected function getIdentifiers(UpsertableContract $upsertable): array|string|int|null
    {
        return $upsertable->getIdentifier();
    }

    protected function getRulesForCreate(): array
    {
        return $this->getRules();
    }

    /**
     * @param  TModel  $model
     */
    protected function getRulesForUpdate(ModelContract|Model $model): array
    {
        return $this->getRules();
    }

    /**
     * @param  TUpsertable  $upsertable
     */
    protected function getUpdateAttributes(UpsertableContract $upsertable): array
    {
        return $upsertable->forUpdate();
    }

    /**
     * @param  TUpsertable  $upsertable
     * @return TModel
     */
    protected function handleCreateModel(UpsertableContract $upsertable): ModelContract|Model
    {
        $this->creating = true;
        $attributes = $this->getCreateAttributes($upsertable);
        $this->validate($attributes, $this->getRulesForCreate());
        $m = $this->getModelInstance()->newInstance($attributes);
        $this->authorize($m, PolicyType::UPSERT);
        if (($this->saved = $m->save()) === false) {
            $this->getMessages()->add('flash:danger', 'Unable to save new model.');
        }

        return $m;
    }

    /**
     * @param  TModel  $model
     * @param  TUpsertable  $upsertable
     * @return TModel
     */
    protected function handleUpdateModel(
        ModelContract|Model $model,
        UpsertableContract $upsertable
    ): ModelContract|Model {
        if (! $this->shouldUpdateModel($model)) {
            return $model;
        }
        $attributes = $this->getUpdateAttributes($upsertable);
        $this->validate($attributes, $this->getRulesForUpdate($model));
        $model->fill($attributes);
        $this->authorize($model, PolicyType::UPSERT);
        if (($this->saved = $model->save()) === false) {
            $this->getMessages()->add('flash:danger', 'Unable to update model.');
        }

        return $model;
    }

    /**
     * @param  TModel  $model
     * @param  TUpsertable  $upsertable
     * @return TModel
     */
    protected function postExecuteFromUpsertable(
        Model|ModelContract $model,
        UpsertableContract $upsertable
    ): Model|ModelContract {
        return $model;
    }

    /**
     * @param  TUpsertable  $upsertable
     * @return TUpsertable
     */
    protected function preExecuteFromUpsertable(UpsertableContract $upsertable): UpsertableContract
    {
        return $upsertable;
    }

    /**
     * @param  TModel|null  $model
     */
    protected function resultFromModel(Model|ModelContract|null $model): Contracts\Results\SaveResult
    {
        return new SaveResult($model, $this->saved, $this->creating, $this->getMessages());
    }

    /**
     * @param  TModel  $model
     */
    protected function shouldUpdateModel(ModelContract|Model $model): bool
    {
        return true;
    }

    protected function shouldUpsert(): bool
    {
        return $this->shouldUpsert;
    }

    /**
     * @param  TUpsertable  $upsertable
     * @return TModel
     */
    protected function upsertModelFromUpsertable(UpsertableContract $upsertable): Model|ModelContract
    {
        return $this->findOrCreateFromUpsertable($upsertable);
    }
}
