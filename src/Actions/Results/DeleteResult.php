<?php

namespace Smorken\Domain\Actions\Results;

use Illuminate\Contracts\Support\MessageBag;

class DeleteResult implements \Smorken\Domain\Actions\Contracts\Results\DeleteResult
{
    public function __construct(
        public string|int|array $id,
        public bool $deleted,
        public MessageBag $messages
    ) {}

    public function idToString(): string
    {
        if (is_array($this->id)) {
            return '['.implode(', ', $this->id).']';
        }

        return $this->id;
    }
}
