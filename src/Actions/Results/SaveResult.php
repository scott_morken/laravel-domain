<?php

namespace Smorken\Domain\Actions\Results;

use Illuminate\Contracts\Support\MessageBag;

class SaveResult implements \Smorken\Domain\Actions\Contracts\Results\SaveResult
{
    public MessageBag $messages;

    public function __construct(
        public mixed $model,
        public bool $saved,
        public bool $creating,
        null|array|MessageBag $messages
    ) {
        $this->ensureMessageBagFromMessages($messages);
    }

    protected function ensureMessageBagFromMessages(null|array|MessageBag $messages): void
    {
        if ($messages instanceof MessageBag) {
            $this->messages = $messages;

            return;
        }
        $messages = is_array($messages) ? $messages : [];
        $this->messages = new \Illuminate\Support\MessageBag($messages);
    }
}
