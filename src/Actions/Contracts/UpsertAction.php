<?php

namespace Smorken\Domain\Actions\Contracts;

use Smorken\Domain\Actions\Contracts\Results\SaveResult;

interface UpsertAction extends Action
{
    public function __invoke(Upsertable $upsertable): SaveResult;
}
