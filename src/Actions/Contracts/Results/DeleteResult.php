<?php

namespace Smorken\Domain\Actions\Contracts\Results;

/**
 * @property string|int|array $id
 * @property bool $deleted
 * @property \Illuminate\Contracts\Support\MessageBag $messages
 */
interface DeleteResult extends Result
{
    public function idToString(): string;
}
