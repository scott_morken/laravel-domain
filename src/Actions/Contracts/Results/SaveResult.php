<?php

namespace Smorken\Domain\Actions\Contracts\Results;

/**
 * @property mixed $model
 * @property bool $saved
 * @property bool $creating
 * @property \Illuminate\Contracts\Support\MessageBag $messages
 */
interface SaveResult extends Result {}
