<?php

declare(strict_types=1);

namespace Smorken\Domain\Actions\Contracts\Support;

interface KeyConverter
{
    public function convert(string $key): string;
}
