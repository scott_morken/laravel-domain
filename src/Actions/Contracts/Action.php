<?php

namespace Smorken\Domain\Actions\Contracts;

use Illuminate\Contracts\Support\MessageBag;

interface Action
{
    public function getMessages(): MessageBag;
}
