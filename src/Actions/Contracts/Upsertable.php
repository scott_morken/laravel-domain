<?php

declare(strict_types=1);

namespace Smorken\Domain\Actions\Contracts;

use Illuminate\Http\Request;
use Smorken\Domain\Actions\Contracts\Support\KeyConverter;
use Smorken\Model\Contracts\Model;

interface Upsertable
{
    public function forCreate(array $skip = [], bool $includeIdentifier = false): array;

    public function forUpdate(array $skip = []): array;

    public function getAttributes(): array;

    public function getIdentifier(): array|int|string|null;

    public function setAttribute(string $key, mixed $value): self;

    public function setAttributes(array $attributes): self;

    public function setKeyConverter(KeyConverter $keyConverter): self;

    public function setKeyConverterClass(string $keyConverterClass): self;

    public function toArray(bool $includeIdentifier = false): array;

    public static function fromArray(array $data, array|int|string|null $identifier): static;

    public static function fromModel(Model $model): static;

    public static function fromRequest(Request $request, array|int|string|null $identifier): static;
}
