<?php

namespace Smorken\Domain\Actions\Contracts;

use Smorken\Domain\Actions\Contracts\Results\DeleteResult;

interface DeleteAction extends Action
{
    public function __invoke(string|int|array $id, bool $authorizeData = true): DeleteResult;

    public function delete(mixed $model, bool $authorizeData = true): DeleteResult;
}
