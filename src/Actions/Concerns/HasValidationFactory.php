<?php

namespace Smorken\Domain\Actions\Concerns;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Contracts\Validation\Validator;

/**
 * @property class-string $rulesProvider
 * @property array $rules
 */
trait HasValidationFactory
{
    protected static Factory $validationFactory;

    public static function setValidationFactory(Factory $factory): void
    {
        self::$validationFactory = $factory;
    }

    protected function fromRulesProvider(): array
    {
        try {
            $cls = $this->getRulesProviderClassString();
            if ($cls) {
                return $cls::rules();
            }

            return [];
        } catch (\Throwable) {
            return [];
        }
    }

    protected function getRules(): array
    {
        if (property_exists($this, 'rules')) {
            return $this->rules;
        }
        if (property_exists($this, 'rulesProvider')) {
            return $this->fromRulesProvider();
        }

        return [];
    }

    protected function getRulesProviderClassString(): ?string
    {
        if (property_exists($this, 'rulesProvider')) {
            return $this->rulesProvider;
        }

        return null;
    }

    protected function getValidationAttributes(): array
    {
        return [];
    }

    protected function getValidationFactory(): Factory
    {
        return self::$validationFactory;
    }

    protected function getValidationMessages(): array
    {
        return [];
    }

    protected function makeValidator(array $data, array $rules): Validator
    {
        return $this->getValidationFactory()
            ->make($data, $rules, $this->getValidationMessages(), $this->getValidationAttributes());
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validate(array $data, array $rules): void
    {
        $this->makeValidator($data, $rules)->validate();
    }
}
