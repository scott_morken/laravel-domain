<?php

namespace Smorken\Domain\Actions;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Smorken\Model\Contracts\Model;

/**
 * @template TModel of EloquentModel|Model
 */
abstract class ActionWithEloquent extends Action
{
    /**
     * @param  TModel  $model
     */
    public function __construct(protected EloquentModel|Model $model)
    {
        parent::__construct();
    }

    /**
     * @return TModel
     */
    protected function eloquentModel(): EloquentModel|Model
    {
        return $this->model;
    }

    /**
     * @return TModel
     */
    protected function getModelInstance(): EloquentModel|Model
    {
        return $this->eloquentModel()->newInstance();
    }
}
