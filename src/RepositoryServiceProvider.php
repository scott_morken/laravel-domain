<?php

namespace Smorken\Domain;

use Illuminate\Contracts\Foundation\Application;
use Smorken\Domain\Factories\RepositoryFactory;

class RepositoryServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
        RepositoryFactory::addRepositories($this->app['config']->get('sm-repositories', []));
    }

    public function register(): void
    {
        RepositoryFactory::setApplication($this->app);
        $this->registerRepositories();
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/repositories.php';
        $this->mergeConfigFrom($config, 'sm-repositories');
        $this->publishes([$config => config_path('sm-repositories.php')], 'config');
    }

    protected function registerRepositories(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-repositories', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }
}
