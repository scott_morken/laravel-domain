<?php

return [
    'domain_namespace' => 'Domain\\',
    'domain_factory_namespace' => 'Database\\Factories\\Eloquent\\',
    'base_factory_namespace' => 'Database\\Factories\\',
];
