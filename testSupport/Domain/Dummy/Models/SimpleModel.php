<?php

declare(strict_types=1);

namespace Domain\Dummy\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Smorken\Domain\Models\BaseEloquentModel;

class SimpleModel extends BaseEloquentModel
{
    use HasFactory;
}
