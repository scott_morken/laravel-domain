<?php

declare(strict_types=1);

namespace Domain\Dummy\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Smorken\Domain\Models\BaseEloquentModel;

class DummyModel extends BaseEloquentModel
{
    use HasFactory;
}
