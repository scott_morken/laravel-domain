<?php

declare(strict_types=1);

namespace Database\Factories\Eloquent\Dummy;

use Illuminate\Database\Eloquent\Factories\Factory;

class SimpleModelFactory extends Factory
{
    public function definition(): array
    {
        return [];
    }
}
